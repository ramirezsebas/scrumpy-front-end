import { createTheme, ThemeProvider } from '@material-ui/core';
import Splash from './pages/Splash';
import { UsuarioProvider } from './data/providers/usuario/usuario-context';
import { MiembroProvider } from './data/providers/miembro/miembro-context';
import { RolProvider } from './data/providers/rol/rol-context';
import { ProyectoProvider } from './data/providers/proyecto/proyecto-context';
import { AuthProvider } from './data/providers/auth/auth-context';
import { ErrorProvider } from './data/providers/error/error-context';
import { UserStoryProvider } from './data/providers/userstory/user-story-context';
import { CommentProvider } from "./data/providers/comment/comment-context";
import { SprintProvider } from './data/providers/sprint/sprint-context';
import { ProjectRolProvider } from './data/providers/rolProyecto/rolProyecto-context';
import { ActivityProvider } from './data/providers/activity/activity-context';
import { EventUSProvider } from './data/providers/eventUS/eventUS-context';

/**
 * Creamos el tema para los estilos de la app
 */
const theme = createTheme({
  // Configuramos la paleta de colores
  palette: {
    primary: {
      main: '#6200EE',
    },
    secondary: {
      main: '#302929',
      light: '#595151',
      dark: '#070000',
    },

  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <AuthProvider>
        <ErrorProvider>
          <RolProvider>
            <UsuarioProvider>
              <ProyectoProvider>
                <MiembroProvider>
                  <ProjectRolProvider>
                    <SprintProvider>
                      <UserStoryProvider>
                        <CommentProvider>
                          <ActivityProvider>
                            <EventUSProvider> 
                              <Splash />
                            </EventUSProvider>
                          </ActivityProvider>
                        </CommentProvider>
                      </UserStoryProvider>
                    </SprintProvider>
                  </ProjectRolProvider>
                </MiembroProvider>
              </ProyectoProvider>
            </UsuarioProvider>
          </RolProvider>
        </ErrorProvider>
      </AuthProvider>
    </ThemeProvider>
  );
}

export default App;
