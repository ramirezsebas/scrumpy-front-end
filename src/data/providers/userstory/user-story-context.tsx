import { createContext, Dispatch, FC, useContext, useEffect } from 'react';
import { useReducer } from 'react';
import userStoryScrumpyAPI from '../../../api/userStoryScrumpyAPI';
import { Action } from '../../../shared/interfaces/action.interface';
import { UserStory, userStoryInitializer } from '../../../shared/interfaces/user_story.interface';
import { getUserStories, setLoaded, setLoading } from './user-story-action-creators';
import { UserStoryActionType } from './user-story-types';
import { ProyectoContext } from '../../providers/proyecto/proyecto-context';

type UserStoryState = {
    userStories: UserStory[];
    currentUS: UserStory;
    loading: boolean;
};

export type PayloadType = UserStory | UserStory[] | boolean;

type UserStoryContextType = {
    state: UserStoryState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState: UserStoryState = {
    userStories: [],
    currentUS: userStoryInitializer,
    loading: false,
};

const initialContext: UserStoryContextType = {
    state: initialState,
    dispatch: () => { },
}

export const UserStoryContext = createContext<UserStoryContextType>(initialContext);

const reducer = (state: UserStoryState, action: Action<PayloadType>): UserStoryState => {
    const { userStories: currentUserStores } = state;
    switch (action.type) {
        case UserStoryActionType.GET_USER_STORIES:
            return {
                ...state,
                userStories: [...action.payload as UserStory[]]
            }
        case UserStoryActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case UserStoryActionType.ADD_USER_STORY:
            return {
                ...state,
                userStories: [...currentUserStores, { ...action.payload as UserStory }]
            }
        case UserStoryActionType.UPDATE_USER_STORY:
            let actualizacion: UserStory = { ...action.payload as UserStory };
            let usuariosActualizados = currentUserStores.map(userStory => {
                if (userStory.id === actualizacion.id) {
                    return {
                        ...userStory,
                        ...actualizacion,
                    }
                }
                return { ...userStory };
            });
            return {
                ...state,
                userStories: [...usuariosActualizados],
                currentUS: state.currentUS.id === actualizacion.id ? actualizacion : state.currentUS,
            }
        case UserStoryActionType.SET_CURRENT_US:
            return {
                ...state,
                currentUS: {...action.payload as UserStory}
            }
        default:
            return {
                ...state
            };
    }
}

export const UserStoryProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { currentProject } } = useContext(ProyectoContext);
    useEffect(() => {

        (async () => {
            const token = localStorage.getItem('token');
            try {
                dispatch(setLoading());
                let idProject = currentProject.id;
                let userStories = await userStoryScrumpyAPI.getUserStories(token as string, idProject);
                console.log(userStories);
                dispatch(getUserStories(userStories));
            } catch (error) {
                console.log(error);
            } finally {
                dispatch(setLoaded());
            }
        })();

    }, [currentProject]);

    return (
        <UserStoryContext.Provider value={{ state, dispatch }}>
            {children}
        </UserStoryContext.Provider>
    );
}
