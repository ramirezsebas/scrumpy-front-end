import { Dispatch, useContext } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import { UserStoryActionType } from "./user-story-types";
import userStoryScrumpyAPI from "../../../api/userStoryScrumpyAPI";
import { UserStory } from "../../../shared/interfaces/user_story.interface";
import { PayloadType } from "./user-story-context";

export const getUserStories = (userStories: UserStory[]) => ({
    type: UserStoryActionType.GET_USER_STORIES,
    payload: userStories,
})

export const setLoading = () => ({
    type: UserStoryActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: UserStoryActionType.LOADING,
    payload: false,
})

export const addUserStory = (userStory: UserStory) => ({
    type: UserStoryActionType.ADD_USER_STORY,
    payload: userStory,
})

export const updateUserStory = (userStory: UserStory) => ({
    type: UserStoryActionType.UPDATE_USER_STORY,
    payload: userStory,
})

export const setCurrentUS = (userStory: UserStory) => ({
    type: UserStoryActionType.SET_CURRENT_US,
    payload: userStory,
})

export const postUserStory = async (dispatch: Dispatch<Action<PayloadType>>, userStory: UserStory) => {
    dispatch(setLoading());
    try {
        console.log(userStory);
        let newUserStory = await userStoryScrumpyAPI.postUserStory(userStory.project, userStory);
        dispatch(addUserStory(newUserStory));
    } catch (error: any) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}

export const putUserStory = async (dispatch: Dispatch<Action<PayloadType>>, userStory: Partial<UserStory>, idProject: number) => {
    dispatch(setLoading());
    try {
        let userStoryActualizado = await userStoryScrumpyAPI.putUserStory(idProject, userStory);
        dispatch(updateUserStory(userStoryActualizado));
    } catch (error: any) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}