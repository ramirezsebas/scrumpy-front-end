import { Dispatch } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import { Member } from "../../../shared/interfaces/miembro.interface";
import { PayloadType } from "../miembro/miembro-context";
import { MeimbroActionType } from "./miembro-action-types";
import equipoAPI from '../../../api/memberScrumpyAPI';
import { MemberRaw } from "../../../shared/interfaces/miembro-raw.interface";

export const getAllMiembros = (miembros: Member[]) => ({
    type: MeimbroActionType.GET_ALL_MIEMBROS,
    payload: miembros,
})

export const setLoading = () => ({
    type: MeimbroActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: MeimbroActionType.LOADING,
    payload: false,
})

export const addMiembro = (miembro: Member) => ({
    type: MeimbroActionType.ADD_MIEMBRO,
    payload: miembro,
})

export const updateMiembro = (miembro: Member) => ({
    type: MeimbroActionType.UPDATE_MIEMBRO,
    payload: miembro,
})

export const deleteMiembroAux = (miembro: Member) => ({
    type: MeimbroActionType.DELETE_MIEMBRO,
    payload: miembro,
})

export const setCurrentMember = (miembro: Member) => ({
    type: MeimbroActionType.SET_CURRENT_MEMBER,
    payload: miembro,
})

export const postMiembro = async (dispatch: Dispatch<Action<PayloadType>>, miembro: Member, project_id: number) => {
    dispatch(setLoading());
    try {
        let miembroAux: MemberRaw = {
            project: miembro.project.id,
            user: miembro.user.email,
            projectRole: miembro.projectRole.id,
            is_active: miembro.is_active,
            availability: miembro.availability,
        };
        let miembroNuevo = await equipoAPI.postMiembro(project_id, miembroAux);
        dispatch(addMiembro(miembroNuevo));
    } catch (error) {
        console.log(error);
    } finally {
        dispatch(setLoaded())
    }
}

export const putMiembro = async (dispatch: Dispatch<Action<PayloadType>>, miembro: Member, project_id: number) => {
    dispatch(setLoading());
    try {
        let miembroAux: MemberRaw = {
            project: miembro.project.id,
            user: miembro.user.email,
            projectRole: miembro.projectRole.id,
            is_active: miembro.is_active,
            availability: miembro.availability,
        };
        let miembroUpdate = await equipoAPI.putMiembro(project_id, miembro.id, miembroAux);
        dispatch(updateMiembro(miembroUpdate));
    } catch (error) {
        console.log(error);
    } finally {
        dispatch(setLoaded())
    }
}

export const deleteMiembro = async (dispatch: Dispatch<Action<PayloadType>>, miembro: Member, project_id: number) => {
    dispatch(setLoading());
    try {
        await equipoAPI.deleteMiembro(project_id, miembro.id);
        dispatch(deleteMiembroAux(miembro));
    } catch (error) {
        console.log(error);
    } finally {
        dispatch(setLoaded())
    }
}