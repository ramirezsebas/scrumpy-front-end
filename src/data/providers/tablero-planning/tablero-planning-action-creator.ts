import { UserStory } from "../../../shared/interfaces/user_story.interface"
import { TableroPlanningActionType } from "./tablero-planning-action-types"

export const getSprintBacklog = (userStories: UserStory[]) => ({
    type: TableroPlanningActionType.GET_SPRINT_BACKLOG,
    payload: userStories,
})

export const getProductBacklog = (userStories: UserStory[]) => ({
    type: TableroPlanningActionType.GET_PRODUCT_BACKLOG,
    payload: userStories,
})
