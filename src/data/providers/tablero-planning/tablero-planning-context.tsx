import { createContext, Dispatch, FC, useContext, useEffect, useReducer, useState } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import { UserStory } from "../../../shared/interfaces/user_story.interface";
import { AuthContext } from "../auth/auth-context";
import { UserStoryContext } from "../userstory/user-story-context";
import { TableroPlanningActionType } from "./tablero-planning-action-types";

export type TableroPlanningState = {
    productBacklog: UserStory[],
    sprintBacklog: UserStory[],
};

export type PayloadType = UserStory[];

type TableroContextType = {
    state: TableroPlanningState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState = {
    productBacklog: [],
    sprintBacklog: []
}

const initialContext: TableroContextType = {
    state: initialState,
    dispatch: () => { },
}

export const TableroContext = createContext<TableroContextType>(initialContext);

const reducer = (state: TableroPlanningState, action: Action<PayloadType>): TableroPlanningState => {
    const { productBacklog: currentProductBacklog, sprintBacklog: currentSprintBacklog } = state;
    switch (action.type) {
        case TableroPlanningActionType.GET_PRODUCT_BACKLOG:
            return {
                ...state,
                productBacklog: [...action.payload as UserStory[]]
            }
        case TableroPlanningActionType.GET_SPRINT_BACKLOG:
            return {
                ...state,
                sprintBacklog: [...action.payload as UserStory[]]
            }
        default:
            return {
                ...state
            };
    }
}

export const ProyectoProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { userStories }, dispatch: storyDispatch } = useContext(UserStoryContext);

    const { state: { auth: { loggedIn } } } = useContext(AuthContext);

    const [productBacklog, setProductBacklog] = useState<UserStory[]>([]);
    const [sprintBacklog, setSprintBacklog] = useState<UserStory[]>([]);

    useEffect(() => {
        setProductBacklog(userStories.filter(us => us.sprint === null));
        setSprintBacklog(userStories.filter(us => us.sprint !== null));
    }, [userStories]);



    useEffect(() => {

        (async () => {
            const token = localStorage.getItem('token');
            // try {
            //     dispatch(setLoading());
            //     let proyectos = await proyectoScrumpyAPI.getProyectos(token as string);
            //     dispatch(getProyectos(proyectos));
            // } catch (error: any) {
            //     console.log(error.message);
            // } finally {
            //     dispatch(setLoaded());
            // }
        })();

    }, [loggedIn]);

    return (
        <TableroContext.Provider value={{ state, dispatch }}>
            {children}
        </TableroContext.Provider>
    );
}

