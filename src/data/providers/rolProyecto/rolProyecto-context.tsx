import { createContext, Dispatch, FC, useCallback, useContext, useEffect, useReducer } from 'react';
import ProjectRolScrumpyAPI from '../../../api/projectRolScrumpyAPI';
import { ProjectRolActionType } from './rolProyecto-actions-types'
import { Action } from '../../../shared/interfaces/action.interface';
import { Permiso, permisoInitializer} from '../../../shared/interfaces/permiso.interface';
import { getRoles, setLoaded, setLoading, getPermisosAPI } from './rolProyecto-actions-creators'
import { AuthContext } from '../auth/auth-context';
import { ProjectRole } from '../../../shared/interfaces/projectRole.interface';
import { ProyectoContext } from '../proyecto/proyecto-context';
import { MiembroContext } from '../miembro/miembro-context';
import { Member } from '../../../shared/interfaces/miembro.interface';
import equipoAPI from '../../../api/memberScrumpyAPI';
import { getAllMiembros, updateMiembro } from '../miembro/miembro-action-creators';

type RolState = {
    roles: ProjectRole[];
    loading: boolean;
    permisos: Permiso[];
}

type RolContextType = {
    state: RolState,
    dispatch: Dispatch<Action<ProjectRole[] | ProjectRole | boolean | Permiso[]>>
    hasPermission: (code: number) => boolean;
    getPermissionById: (id: number) => Permiso | undefined;
}

const initialState: RolState = {
    roles: [],
    loading: false,
    permisos: [],
}

const initialContext: RolContextType = {
    state: initialState,
    dispatch: () => { },
    hasPermission: (code: number) => false,
    getPermissionById: (id: number) => ({...permisoInitializer}),
}

export const ProjectRolContext = createContext<RolContextType>(initialContext);

const reducer = (state: RolState, action: Action<ProjectRole[] | boolean | ProjectRole | Permiso[]>): RolState => {
    switch (action.type) {
        case ProjectRolActionType.GET_ROLES:
            return {
                ...state,
                roles: [...action.payload as ProjectRole[]],
            }
        case ProjectRolActionType.ADD_ROL:
            let { roles: currentRoles } = state;
            return {
                ...state,
                roles: [...currentRoles, { ...action.payload as ProjectRole }]
            }
        case ProjectRolActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case ProjectRolActionType.GET_PERMISOS:
            return {
                ...state,
                permisos: [...action.payload as Permiso[]],
            }
        case ProjectRolActionType.UPDATE_ROL:
            let actualizacion = {...action.payload as ProjectRole};
            let updatedRoles = state.roles.map(rol => {
                if(rol.id === actualizacion.id)
                    return {
                        ...rol,
                        ...actualizacion,
                    }
                return {
                    ...rol
                }
            });
            return {
                ...state,
                roles: [...updatedRoles]
            }
        case ProjectRolActionType.DELETE_ROL:
                const rolToDelete = action.payload as ProjectRole;
                return {
                    ...state,
                    roles: state.roles.filter(rol => rol.id != rolToDelete.id),
                }
        default:
            return state;
    }
}


export const ProjectRolProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { auth: { usuario, loggedIn } } } = useContext(AuthContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { miembros }, dispatch:miembroDispatch } = useContext(MiembroContext);

    useEffect(() => {
        // Hacemos el GET request a la API
        (async () => {
            try {
                dispatch(setLoading());
                let permisos = await getPermisosAPI(dispatch);
                console.log(JSON.stringify(permisos));
                let roles = await ProjectRolScrumpyAPI.getRoles(currentProject.id as number);
                dispatch(getRoles(roles));
            } catch (error:any) {
                console.log(error.message);
            } finally {
                dispatch(setLoaded());
            }
        })();
    }, [loggedIn, currentProject]);

    useEffect(() => {
        (async () => {
            let miembros = await equipoAPI.getAllMiembros(currentProject.id as number);
            miembroDispatch(getAllMiembros(miembros));
        })();
    }, [state]);

    const getRolById = (id: number) => {
        return state.roles.find(rol => rol.id === id);
    }

    const getPermissionById = (id: number) => {
        return state.permisos.find(permission => permission.id === id);
    }

    const getMiembroByUserId = (userId: number) => {
        return miembros.find(miembro => miembro.user.id === userId);
    }

    const hasPermission = useCallback((code: number): boolean => {
        console.log('Chequeando permisos de: ' + JSON.stringify(usuario));
        const miembro = getMiembroByUserId(usuario.id) as Member || {};
        const rol = miembro.projectRole;
        let result = false;
        if(rol){
            for(let permissionId of rol?.permissions) {
                let permission = getPermissionById(permissionId);
                result = !!permission ? result || permission.code === code : result;
            }
        }
        return result;
    }, [miembros, state, loggedIn]);

    return (
        <ProjectRolContext.Provider value={{
            state,
            dispatch,
            hasPermission,
            getPermissionById,
        }}>
            {children}
        </ProjectRolContext.Provider>
    );
}
