import { Dispatch } from "react"
import ProjectRolScrumpyAPI from "../../../api/projectRolScrumpyAPI"
import { Action } from "../../../shared/interfaces/action.interface"
import { ProjectRole } from "../../../shared/interfaces/projectRole.interface"
import { Permiso } from "../../../shared/interfaces/permiso.interface"
import { ProjectRolActionType } from "./rolProyecto-actions-types"

export const getRoles = (roles: ProjectRole[]) =>{
    return {
        type: ProjectRolActionType.GET_ROLES,
        payload: roles,
    }

}

export const addRol = (rol: ProjectRole) =>{
    return {
        type: ProjectRolActionType.ADD_ROL,
        payload: rol,
    }
}

export const setLoading = () =>({
    type: ProjectRolActionType.LOADING,
    payload: true,
})

export const setLoaded = () =>({
    type: ProjectRolActionType.LOADING,
    payload: false,
})

export const updateRol = (rol: ProjectRole) => ({
    type: ProjectRolActionType.UPDATE_ROL,
    payload: rol,
});

export const deleteRolAction = (rol: ProjectRole) => ({
    type: ProjectRolActionType.DELETE_ROL,
    payload: rol,
});

export const postRol = async (dispatch: Dispatch<Action<ProjectRole|ProjectRole[]|boolean>>, rol: ProjectRole, id_proyecto: number) => {
    dispatch(setLoading());
    try {
        let rolNuevo = await ProjectRolScrumpyAPI.postRol(id_proyecto, rol);
        dispatch(addRol(rolNuevo));
    } catch(error) {
        console.log(error);
    } finally {
        dispatch(setLoaded())
    }
}

export const getPermisos = (permisos: Permiso[]) =>{
    return {
        type: ProjectRolActionType.GET_PERMISOS,
        payload: permisos,
    }

}

export const getPermisosAPI = async (dispatch: Dispatch<Action<ProjectRole|ProjectRole[]|boolean | Permiso[]>>) => {
    try {
        let permisos = await ProjectRolScrumpyAPI.getPermisos();
        console.log(JSON.stringify(permisos));
        dispatch(getPermisos(permisos));
    } catch(error) {
        console.log(error);
    }
}

export const putRol = async (dispatch: Dispatch<Action<ProjectRole|ProjectRole[]|boolean | Permiso[]>>, rol: ProjectRole, id_proyecto: number) => {
    try {
        let updatedRol = await ProjectRolScrumpyAPI.putRol(id_proyecto, rol);
        dispatch(updateRol(updatedRol));
    } catch(error) {

    }
}

export const deleteRol = async (dispatch: Dispatch<Action<ProjectRole|ProjectRole[]|boolean | Permiso[]>>, rol: ProjectRole, id_proyecto: number) => {
    dispatch(setLoading());
    await ProjectRolScrumpyAPI.deleteRol(id_proyecto, rol);
    dispatch(deleteRolAction(rol));
    dispatch(setLoaded())
}
