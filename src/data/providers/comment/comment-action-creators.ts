import { CommentActionType } from "./comment-action-types"
import { Comment } from "../../../shared/interfaces/comment.interface";
import { Dispatch } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import commentAPI from "../../../api/commentScrumpyAPI";
import { CommentRaw } from "../../../shared/interfaces/comment-raw.interface";
import { Proyecto } from "../../../shared/interfaces/proyecto.interface";

export const setLoading = () => ({
    type: CommentActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: CommentActionType.LOADING,
    payload: false,
})

export const getAllComments = (comments: Comment[]) => ({
    type: CommentActionType.GET_COMMENTS,
    payload: comments,
})

export const addComment = (comment: Comment) => ({
    type: CommentActionType.ADD_COMMENT,
    payload: comment,
})

export const deleteCommentAction = (comment: Comment) => ({
    type: CommentActionType.DELETE_COMMENT,
    payload: comment,
})

export const updateComment = (comment: Comment) => ({
    type: CommentActionType.UPDATE_COMMENT,
    payload: comment,
})

export const postComment = async (dispatch: Dispatch<Action<Comment[] | Comment | boolean>>, idProject:number, idUS:number, comment: CommentRaw) => {
    dispatch(setLoading());
    try {
        const newComment = await commentAPI.postComment(idProject, idUS, comment);
        dispatch(addComment(newComment));
    } catch(error) {
        console.log(error);
    } finally {
        dispatch(setLoaded());
    }
}

export const deleteComment = async (dispatch: Dispatch<Action<Comment[] | Comment | boolean>>, comment: Comment) => {
    const project: any = comment.user_story.project;
    dispatch(setLoading());
    try {
        await commentAPI.deleteComment(project.id, comment.user_story.id, comment.id);
        dispatch(deleteCommentAction(comment));
    } catch(error) {
        console.log(error);
    } finally {
        dispatch(setLoaded());
    }
}

export const putComment = async (dispatch: Dispatch<Action<Comment[] | Comment | boolean>>, idProject:number, idUS:number, comment: Partial<Comment>) => {
    dispatch(setLoading());
    try {
        const updatedComment = await commentAPI.putComment(idProject, idUS, comment);
        dispatch(updateComment(updatedComment));
    } catch(error) {
        console.log(error);
    } finally {
        dispatch(setLoaded());
    }
}