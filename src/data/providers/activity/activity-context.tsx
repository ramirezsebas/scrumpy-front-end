import { Dispatch, useEffect, createContext, useReducer, FC, useContext } from 'react';
import { Action } from '../../../shared/interfaces/action.interface';
import { ActivityActionType } from "./activity-action-types";
import { Activity } from "../../../shared/interfaces/activity.interface"
import ActivityScrumpyAPI from "../../../api/activityScrumpyAPI";
import { setLoading, setLoaded, getAllActivities } from "./activity-action-creators";
import { ProyectoContext } from '../proyecto/proyecto-context';
import { UserStoryContext } from '../userstory/user-story-context'

type ActivityState = {
    activities: Activity[],
    loading: boolean;
}

export type PayloadType = Activity[] | Activity | boolean;

type ActivityContextType = {
    state: ActivityState;
    dispatch: Dispatch<Action<PayloadType>>;
}

const initialState: ActivityState = {
    activities: [],
    loading: false,
}

const initialContext: ActivityContextType = {
    state: initialState,
    dispatch: () => { },
}

export const ActivityContext = createContext<ActivityContextType>(initialContext);

const reducer = (state: ActivityState, action: Action<PayloadType>): ActivityState => {
    const { activities: currentActivitys } = state;
    switch (action.type) {
        case ActivityActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case ActivityActionType.GET_ACTIVITIES:
            return {
                ...state,
                activities: action.payload as Activity[]
            }
        case ActivityActionType.ADD_ACTIVITY:
            return {
                ...state,
                activities: [...currentActivitys, { ...action.payload as Activity }]
            }
        case ActivityActionType.UPDATE_ACTIVITY:
            let actualizacion: Activity = { ...action.payload as Activity };
            let activitiesActualizados = currentActivitys.map(activity => {
                if (activity.id === actualizacion.id) {
                    return {
                        ...activity,
                        ...actualizacion,
                    }
                }
                return { ...activity };
            });
            return {
                ...state,
                activities: [...activitiesActualizados]
            }
        default:
            return {
                ...state
            }
    }
}

export const ActivityProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);

    useEffect(() => {
        (async () => {
            try {
                dispatch(setLoading());
                let activities = await ActivityScrumpyAPI.getAllActivities(currentProject.id as number, currentUS.id) as Activity[];
                dispatch(getAllActivities(activities));
            } catch (error) {
                console.log(error);
            } finally {
                dispatch(setLoaded());
            }
        })();
    }, [currentProject, currentUS]);

    return (
        <ActivityContext.Provider value={{ state, dispatch }}>
            {children}
        </ActivityContext.Provider>
    );
}