import { ActivityActionType } from "./activity-action-types"
import { Activity } from "../../../shared/interfaces/activity.interface";
import { Dispatch } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import activityAPI from "../../../api/activityScrumpyAPI";
import { Proyecto } from "../../../shared/interfaces/proyecto.interface";

export const setLoading = () => ({
    type: ActivityActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: ActivityActionType.LOADING,
    payload: false,
})

export const getAllActivities = (activities: Activity[]) => ({
    type: ActivityActionType.GET_ACTIVITIES,
    payload: activities,
})

export const addActivity = (activity: Activity) => ({
    type: ActivityActionType.ADD_ACTIVITY,
    payload: activity,
})

export const updateActivity = (activity: Activity) => ({
    type: ActivityActionType.UPDATE_ACTIVITY,
    payload: activity,
})

export const postActivity = async (dispatch: Dispatch<Action<Activity[] | Activity | boolean>>, idProject:number, idUS:number, activity: Activity) => {
    dispatch(setLoading());
    try {
        const newActivity = await activityAPI.postActivity(idProject, idUS, activity);
        dispatch(addActivity(newActivity));
    } catch(error) {
        console.log(error);
    } finally {
        dispatch(setLoaded());
    }
}

export const putActivity = async (dispatch: Dispatch<Action<Activity[] | Activity | boolean>>, idProject:number, idUS:number, activity: Partial<Activity>) => {
    dispatch(setLoading());
    try {
        const updatedActivity = await activityAPI.putActivity(idProject, idUS, activity);
        dispatch(updateActivity(updatedActivity));
    } catch(error) {
        console.log(error);
    } finally {
        dispatch(setLoaded());
    }
}