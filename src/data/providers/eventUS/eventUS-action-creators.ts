import { Dispatch } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import { Proyecto } from "../../../shared/interfaces/proyecto.interface";
import { EventUS } from "../../../shared/interfaces/eventUS.interface";
import { EventUSActionType } from "./eventUS-action-types";

export const setLoading = () => ({
    type: EventUSActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: EventUSActionType.LOADING,
    payload: false,
})

export const getAllEvents = (events: EventUS[]) => ({
    type: EventUSActionType.GET_EVENTS,
    payload: events,
})