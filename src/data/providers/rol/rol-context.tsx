import { createContext, Dispatch, FC, useCallback, useContext, useEffect, useReducer } from 'react';
import RolScrumpyAPI from '../../../api/rolScrumpyAPI';
import { RolActionType } from './rol-actions-types'
import { Action } from '../../../shared/interfaces/action.interface';
import { Permiso, permisoInitializer} from '../../../shared/interfaces/permiso.interface';
import { getRoles, setLoaded, setLoading, getPermisosAPI } from './rol-actions-creators'
import { AuthContext } from '../auth/auth-context';
import { Role } from '../../../shared/interfaces/role.interface';

type RolState = {
    roles: Role[];
    loading: boolean;
    permisos: Permiso[];
}

type RolContextType = {
    state: RolState,
    dispatch: Dispatch<Action<Role[] | Role | boolean | Permiso[]>>
    hasPermission: (code: number) => boolean;
    getPermissionById: (id: number) => Permiso | undefined;
}

const initialState: RolState = {
    roles: [],
    loading: false,
    permisos: [],
}

const initialContext: RolContextType = {
    state: initialState,
    dispatch: () => { },
    hasPermission: (code: number) => false,
    getPermissionById: (id: number) => ({...permisoInitializer}),
}

export const RolContext = createContext<RolContextType>(initialContext);

const reducer = (state: RolState, action: Action<Role[] | boolean | Role | Permiso[]>): RolState => {
    switch (action.type) {
        case RolActionType.GET_ROLES:
            return {
                ...state,
                roles: [...action.payload as Role[]],
            }
        case RolActionType.ADD_ROL:
            let { roles: currentRoles } = state;
            return {
                ...state,
                roles: [...currentRoles, { ...action.payload as Role }]
            }
        case RolActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case RolActionType.GET_PERMISOS:
            return {
                ...state,
                permisos: [...action.payload as Permiso[]],
            }
        case RolActionType.UPDATE_ROL:
            let actualizacion = {...action.payload as Role};
            let updatedRoles = state.roles.map(rol => {
                if(rol.id === actualizacion.id)
                    return {
                        ...rol,
                        ...actualizacion,
                    }
                return {
                    ...rol
                }
            });
            return {
                ...state,
                roles: [...updatedRoles]
            }
        case RolActionType.DELETE_ROL:
                const rolToDelete = action.payload as Role;
                return {
                    ...state,
                    roles: state.roles.filter(rol => rol.id != rolToDelete.id),
                }
        default:
            return state;
    }
}


export const RolProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { auth: { usuario, loggedIn }, } } = useContext(AuthContext);

    useEffect(() => {
        // Hacemos el GET request a la API
        (async () => {
            try {
                dispatch(setLoading());
                let permisos = await getPermisosAPI(dispatch);
                console.log(JSON.stringify(permisos));
                let roles = await RolScrumpyAPI.getRoles();
                dispatch(getRoles(roles));
            } catch (error:any) {
                console.log(error.message);
            } finally {
                dispatch(setLoaded());
            }
        })();
    }, [loggedIn]);

    const getRolById = (id: number) => {
        return state.roles.find(rol => rol.id === id);
    }

    const getPermissionById = (id: number) => {
        return state.permisos.find(permission => permission.id === id);
    }

    const hasPermission = useCallback((code: number): boolean => {
        console.log('Chequeando permisos de: ' + JSON.stringify(usuario));
        const rol = getRolById(usuario.system_role);
        let result = false;
        if(rol){
            for(let permissionId of rol.permissions) {
                let permission = getPermissionById(permissionId);
                result = !!permission ? result || permission.code === code : result;
            }
        }
        return result;
    }, [usuario, state]);

    return (
        <RolContext.Provider value={{
            state,
            dispatch,
            hasPermission,
            getPermissionById,
        }}>
            {children}
        </RolContext.Provider>
    );
}
