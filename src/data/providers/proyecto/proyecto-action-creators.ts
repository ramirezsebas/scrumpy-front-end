import { Dispatch } from "react"
import proyectoScrumpyAPI from "../../../api/proyectoScrumpyAPI"
import { ActionType } from "../../../shared/enums/user-management.enum"
import { Action } from "../../../shared/interfaces/action.interface"
import { Proyecto } from "../../../shared/interfaces/proyecto.interface"

import { ProyectoActionType } from "./proyecto-action-types"
import { PayloadType } from "./proyecto-context"

export const getProyectos = (proyectos: Proyecto[]) => ({
    type: ProyectoActionType.GET_PROYECTOS,
    payload: proyectos,
})

export const setLoading = () => ({
    type: ProyectoActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: ProyectoActionType.LOADING,
    payload: false,
})

export const addProyecto = (proyecto: Proyecto) => ({
    type: ProyectoActionType.ADD_PROYECTO,
    payload: proyecto,
})

export const updateProyecto = (proyecto: Proyecto) => ({
    type: ProyectoActionType.UPDATE_PROYECTO,
    payload: proyecto,
})

export const clearProjects = () => ({
    type: ProyectoActionType.CLEAR_PROJECTS,
})

export const setCurrentProject = (proyecto: Proyecto) => ({
    type: ProyectoActionType.SET_CURRENT_PROJECT,
    payload: proyecto,
});

export const postProyecto = async (dispatch: Dispatch<Action<PayloadType>>, proyecto: Proyecto) => {
    dispatch(setLoading());
    console.log(proyecto);
    try {
        let proyectoNuevo = await proyectoScrumpyAPI.postProyecto(proyecto);
        console.log(proyectoNuevo);
        console.log("Aca en Action")

        dispatch(addProyecto(proyectoNuevo));
    } catch(error:any) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}

export const putProyecto = async (dispatch: Dispatch<Action<PayloadType>>, proyecto: Proyecto, isCurrent?: boolean) => {
    dispatch(setLoading());
    try {
        let proyectoActualizado = await proyectoScrumpyAPI.updateProyecto(proyecto);
        dispatch(updateProyecto(proyectoActualizado));
        if (isCurrent) {
            dispatch(setCurrentProject(proyecto));
        }
    } catch(error:any) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}