import { createContext, Dispatch, FC, useContext, useEffect } from 'react';
import { Action } from '../../../shared/interfaces/action.interface';

import { useReducer } from 'react';
import { ProyectoActionType } from './proyecto-action-types';
import { getProyectos, setLoaded, setLoading } from './proyecto-action-creators';
import proyectoScrumpyAPI from '../../../api/proyectoScrumpyAPI';
import { Proyecto, proyectoInitializer } from '../../../shared/interfaces/proyecto.interface';
import { AuthContext } from '../auth/auth-context';


type ProyectoState = {
    proyectos: Proyecto[];
    currentProject: Proyecto;
    loading: boolean;
};

export type PayloadType = Proyecto | Proyecto[] | boolean;

type ProyectoContextType = {
    state: ProyectoState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState: ProyectoState = {
    proyectos: [],
    currentProject: proyectoInitializer,
    loading: false,
};

const initialContext: ProyectoContextType = {
    state: initialState,
    dispatch: () => { },
}

export const ProyectoContext = createContext<ProyectoContextType>(initialContext);

const reducer = (state: ProyectoState, action: Action<PayloadType>): ProyectoState => {
    const { proyectos: currentProyectos } = state;
    switch (action.type) {
        case ProyectoActionType.GET_PROYECTOS:
            return {
                ...state,
                proyectos: [...action.payload as Proyecto[]]
            }
        case ProyectoActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case ProyectoActionType.ADD_PROYECTO:
            return {
                ...state,
                proyectos: [...currentProyectos, { ...action.payload as Proyecto }]
            }
        case ProyectoActionType.UPDATE_PROYECTO:
            let actualizacion: Proyecto = { ...action.payload as Proyecto };
            let proyectosActualizados = currentProyectos.map(proyecto => {
                if (proyecto.id === actualizacion.id) {
                    return {
                        ...proyecto,
                        ...actualizacion,
                    }
                }
                return { ...proyecto };
            });
            return {
                ...state,
                proyectos: [...proyectosActualizados]
            }
        case ProyectoActionType.SET_CURRENT_PROJECT:
            return {
                ...state,
                currentProject: {...action.payload as Proyecto},
            }
        case ProyectoActionType.CLEAR_PROJECTS:
            return {
                ...state,
                proyectos: [],
            }
        default:
            return {
                ...state
            };
    }
}

export const ProyectoProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const { state:{ auth: { loggedIn } } } = useContext(AuthContext);

    useEffect(() => {

        (async () => {
            const token = localStorage.getItem('token');
            try {
                dispatch(setLoading());
                let proyectos = await proyectoScrumpyAPI.getProyectos(token as string);
                dispatch(getProyectos(proyectos));
            } catch (error: any) {
                console.log(error.message);
            } finally {
                dispatch(setLoaded());
            }
        })();

    }, [loggedIn]);

    return (
        <ProyectoContext.Provider value={{ state, dispatch }}>
            {children}
        </ProyectoContext.Provider>
    );
}

