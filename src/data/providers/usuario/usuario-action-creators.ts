import { Dispatch } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import { Usuario } from "../../../shared/interfaces/usuario.interface";
import { UsuarioActionType } from './usuario-action-types';
import { PayloadType } from './usuario-context';
import usuarioAPI from '../../../api/usuarioScrumpyAPI';
import { UserStory } from "../../../shared/interfaces/user_story.interface";

export const getUsuarios = (usuarios: Usuario[]) => ({
    type: UsuarioActionType.GET_USUARIOS,
    payload: usuarios,
})

export const setLoading = () => ({
    type: UsuarioActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: UsuarioActionType.LOADING,
    payload: false,
})

export const addUsuario = (usuario: Usuario) => ({
    type: UsuarioActionType.ADD_USUARIO,
    payload: usuario,
})

export const updateUsuario = (usuario: Usuario) => ({
    type: UsuarioActionType.UPDATE_USUARIO,
    payload: usuario,
})

export const postUsuario = async (dispatch: Dispatch<Action<PayloadType>>, usuario: Usuario) => {
    dispatch(setLoading());
    try {
        let usuarioNuevo = await usuarioAPI.postUsuario(usuario);
        dispatch(addUsuario(usuarioNuevo));
    } catch(error) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}

export const putUsuario = async (dispatch: Dispatch<Action<PayloadType>>, usuario: Usuario) => {
    dispatch(setLoading());
    try {
        let usuarioActualizado = await usuarioAPI.updateUsuario(usuario);
        dispatch(updateUsuario(usuarioActualizado));
    } catch(error) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}