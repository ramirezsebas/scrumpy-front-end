import React, { createContext, Dispatch, FC, useContext, useEffect } from 'react';
import { useReducer } from 'react';
import usuariosAPI from '../../../api/usuarioScrumpyAPI';
import { Action } from '../../../shared/interfaces/action.interface';
import { UserStory } from '../../../shared/interfaces/user_story.interface';
import { Usuario } from '../../../shared/interfaces/usuario.interface';
import { AuthContext } from '../auth/auth-context';
import { getUsuarios, setLoaded, setLoading } from './usuario-action-creators';
import { UsuarioActionType } from './usuario-action-types';

type UsuarioState = {
    usuarios: Usuario[];
    tasks: UserStory[];
    loading: boolean;
};

export type PayloadType = Usuario | Usuario[] | boolean | UserStory[];

type UsuarioContextType = {
    state: UsuarioState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState: UsuarioState = {
    usuarios: [],
    loading: false,
    tasks: [],
};

const initialContext: UsuarioContextType = {
    state: initialState,
    dispatch: () => { },
}

export const UsuarioContext = createContext<UsuarioContextType>(initialContext);

const reducer = (state: UsuarioState, action: Action<PayloadType>): UsuarioState => {
    const { usuarios:currentUsers } = state;
    switch(action.type) {
        case UsuarioActionType.GET_USUARIOS:
            return {
                ...state,
                usuarios: [...action.payload as Usuario[]]
            }
        case UsuarioActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case UsuarioActionType.ADD_USUARIO:
            return {
                ...state,
                usuarios: [...currentUsers, { ...action.payload as Usuario }]
            }
        case UsuarioActionType.UPDATE_USUARIO:
            let actualizacion: Usuario = {...action.payload as Usuario};
            let usuariosActualizados = currentUsers.map(usuario => {
                if(usuario.id === actualizacion.id) {
                    return {
                        ...usuario,
                        ...actualizacion,
                    }
                }
                return {...usuario};
            });
            return {
                ...state,
                usuarios: [...usuariosActualizados]
            }
        default:
            return {
                ...state
            };
    }
}

export const UsuarioProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { auth: { loggedIn } } } = useContext(AuthContext);

    useEffect(() => {

        (async () => {
            const token = localStorage.getItem('token');
            try {
                dispatch(setLoading());
                let usuarios = await usuariosAPI.getUsuarios(token as string);
                dispatch(getUsuarios(usuarios));
            } catch (error) {
                console.log(error);
            } finally {
                dispatch(setLoaded());
            }
        })();

    }, [loggedIn]);

    return (
        <UsuarioContext.Provider value={{ state, dispatch }}>
            {children}
        </UsuarioContext.Provider>
    );
}
