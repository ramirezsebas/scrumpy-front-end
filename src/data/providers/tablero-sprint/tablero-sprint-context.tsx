import { createContext, Dispatch, FC, useContext, useEffect, useReducer, useState } from "react";
import { Action } from "../../../shared/interfaces/action.interface";
import { UserStory } from "../../../shared/interfaces/user_story.interface";
import { AuthContext } from "../auth/auth-context";
import { UserStoryContext } from "../userstory/user-story-context";
import { TableroSprintActionType } from "./tablero-sprint-action-types";

export type TableroSprintState = {
    todo: UserStory[],
    doing: UserStory[],
    done: UserStory[],
};

export type PayloadType = UserStory[];

type TableroContextType = {
    state: TableroSprintState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState = {
    productBacklog: [],
    sprintBacklog: [],
    todo: [],
    doing: [],
    done: [],
}

const initialContext: TableroContextType = {
    state: initialState,
    dispatch: () => { },
}

export const TableroContext = createContext<TableroContextType>(initialContext);

const reducer = (state: TableroSprintState, action: Action<PayloadType>): TableroSprintState => {
    const { todo: currentTodo, doing: currentDoing, done: currentDone, } = state;
    switch (action.type) {
        case TableroSprintActionType.GET_TODO:
            return {
                ...state,
                todo: [...action.payload as UserStory[]]
            }
        case TableroSprintActionType.GET_DOING:
            return {
                ...state,
                doing: [...action.payload as UserStory[]]
            }
        case TableroSprintActionType.GET_DONE:
            return {
                ...state,
                done: [...action.payload as UserStory[]]
            }
        default:
            return {
                ...state
            };
    }
}

export const ProyectoProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const { state: { userStories }, dispatch: storyDispatch } = useContext(UserStoryContext);

    const { state: { auth: { loggedIn } } } = useContext(AuthContext);

    const [todo, setTodo] = useState<UserStory[]>([]);
    const [doing, setDoing] = useState<UserStory[]>([]);
    const [done, setDone] = useState<UserStory[]>([]);

    useEffect(() => {
        setTodo(userStories.filter(us => us.sprint !== null && us.status === 'TODO'));
        setDoing(userStories.filter(us => us.sprint !== null && us.status === 'DOING'));
        setDone(userStories.filter(us => us.sprint !== null && us.status === 'DONE'));
    }, [userStories]);



    useEffect(() => {

        (async () => {
            const token = localStorage.getItem('token');
            // try {
            //     dispatch(setLoading());
            //     let proyectos = await proyectoScrumpyAPI.getProyectos(token as string);
            //     dispatch(getProyectos(proyectos));
            // } catch (error: any) {
            //     console.log(error.message);
            // } finally {
            //     dispatch(setLoaded());
            // }
        })();

    }, [loggedIn]);

    return (
        <TableroContext.Provider value={{ state, dispatch }}>
            {children}
        </TableroContext.Provider>
    );
}

