export enum TableroSprintActionType {
    GET_TODO = 'GET_TODO',
    GET_DOING = 'GET_DOING',
    GET_DONE = 'GET_DONE',
}