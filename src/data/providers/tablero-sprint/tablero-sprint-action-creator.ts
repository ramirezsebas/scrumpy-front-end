import { UserStory } from "../../../shared/interfaces/user_story.interface"
import { TableroSprintActionType } from "./tablero-sprint-action-types"

export const getTodo = (userStories: UserStory[]) => ({
    type: TableroSprintActionType.GET_TODO,
    payload: userStories,
})

export const getDoing = (userStories: UserStory[]) => ({
    type: TableroSprintActionType.GET_DOING,
    payload: userStories,
})
export const getDone = (userStories: UserStory[]) => ({
    type: TableroSprintActionType.GET_DONE,
    payload: userStories,
})
