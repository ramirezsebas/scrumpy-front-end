import { createContext, Dispatch, FC, useContext, useEffect } from 'react';
import { Action } from '../../../shared/interfaces/action.interface';
import { useReducer } from 'react';
import { SprintActionType } from './sprint-action-types';
import { getSprints, setLoaded, setLoading, setCurrentSprint, setSelectedSprint } from './sprint-action-creators';
import sprintScrumpyAPI from '../../../api/sprintScrumpyAPI';
import { Sprint, sprintInitializer } from '../../../shared/interfaces/sprint.interface';
import { AuthContext } from '../auth/auth-context';
import { ProyectoContext } from '../proyecto/proyecto-context';

type SprintState = {
    sprints: Sprint[];
    currentSprint: Sprint;
    selectedSprint: number;
    loading: boolean;
};
 
export type PayloadType = Sprint | Sprint[] | boolean | number;

type SprintContextType = {
    state: SprintState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState: SprintState = {
    sprints: [],
    currentSprint: sprintInitializer,
    selectedSprint: 0,
    loading: false,
};

const initialContext: SprintContextType = {
    state: initialState,
    dispatch: () => { },
}

export const SprintContext = createContext<SprintContextType>(initialContext);

const reducer = (state: SprintState, action: Action<PayloadType>): SprintState => {
    const { sprints: currentSprints } = state;
    switch (action.type) {
        case SprintActionType.GET_SPRINTS:
            return {
                ...state,
                sprints: [...action.payload as Sprint[]]
            }
        case SprintActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case SprintActionType.ADD_SPRINT:
            return {
                ...state,
                sprints: [...currentSprints, { ...action.payload as Sprint }],
                currentSprint: { ...action.payload as Sprint },
            }
        case SprintActionType.UPDATE_SPRINT:
            let actualizacion: Sprint = { ...action.payload as Sprint };
            let sprintsActualizados = currentSprints.map(sprint => {
                if (sprint.id === actualizacion.id) {
                    return {
                        ...sprint,
                        ...actualizacion,
                    }
                }
                return { ...sprint };
            });
            return {
                ...state,
                sprints: [...sprintsActualizados],
                currentSprint: state.currentSprint.id === actualizacion.id ? actualizacion : state.currentSprint,
            }
        case SprintActionType.SET_CURRENT_SPRINT:
            return {
                ...state,
                currentSprint: { ...action.payload as Sprint },
            }
        case SprintActionType.SET_SELECTED_SPRINT:
            return {
                ...state,
                selectedSprint: action.payload as number,
            }
        default:
            return {
                ...state
            };
    }
}

export const SprintProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const { state: { auth: { loggedIn } } } = useContext(AuthContext);
    const { state: { currentProject } } = useContext(ProyectoContext);

    useEffect(() => {

        (async () => {
            try {
                dispatch(setLoading());
                let sprints = await sprintScrumpyAPI.getSprints(currentProject.id as number);
                let lastIteration = 0;
                let _currentSprint: Sprint = sprintInitializer;
                for (let sprint of sprints) {
                    if (sprint.iteration > lastIteration) {
                        lastIteration = sprint.iteration;
                        _currentSprint = sprint;
                    }
                }
                console.log(_currentSprint);
                dispatch(setCurrentSprint(_currentSprint));
                dispatch(getSprints(sprints));
                dispatch(setSelectedSprint(_currentSprint.id));
            } catch (error: any) {
                console.log(error.message);
            } finally {
                dispatch(setLoaded());
            }
        })();

    }, [loggedIn, currentProject]);

    return (
        <SprintContext.Provider value={{ state, dispatch }}>
            {children}
        </SprintContext.Provider>
    );
}

