import React, {  createContext, useState, FC, SetStateAction, Dispatch } from 'react';
import { Dialog } from '../../../shared/interfaces/dialog.interface';

const initialDialog = {
    title: 'initial',
    body: '',
    open: false,
    buttonLabel: ''
};

type ErrorContextType = {
    dialog: Dialog;
    setDialog: Dispatch<SetStateAction<Dialog>>;
    handleClose: () => void;
}

const initialContext = {
    dialog: initialDialog,
    setDialog: () => {},
    handleClose: () => {},
}

export const ErrorContext = createContext<ErrorContextType>(initialContext);

export const ErrorProvider: FC = ({ children }) => {
    const [dialog, setDialog] = useState<Dialog>(initialDialog);

    const handleClose = () => setDialog({
        ...dialog, open: false
    });

    return (
        <ErrorContext.Provider value={{dialog, setDialog, handleClose}}>
            { children }
        </ErrorContext.Provider>
    );
}