import React, { createContext, Dispatch, FC, useEffect} from 'react';
import { useReducer } from 'react';
import authAPI from '../../../api/authScrumpyAPI';
import { AuthActionType } from './auth-action-types';
import { Action } from '../../../shared/interfaces/action.interface';
import { Auth } from '../../../shared/interfaces/auth.interface';
import { usuarioInitializer } from '../../../shared/interfaces/usuario.interface';

type AuthState = {
    auth: Auth;
    initialized: boolean;
};

type AuthContextType = {
    state: AuthState;
    dispatch: Dispatch<Action<Auth>>;
};

const initialState: AuthState = {
    auth: {
        usuario: usuarioInitializer,
        tasks: [],
        token: '',
        loggedIn: false,
    },
    initialized: true,
};

const initialContext: AuthContextType = {
    state: initialState,
    dispatch: () => {},
}

export const AuthContext = createContext<AuthContextType>(initialContext);

const reducer = (state: AuthState, action: Action<Auth>): AuthState => {
    const { auth: initAuth } = initialState;
    switch(action.type) {
        case AuthActionType.SET_LOGIN_STATE:
            let auth = action.payload as Auth;
            localStorage.setItem('token', auth.token as string)
            return {
                ...state,
                auth: {
                    ...auth,
                    loggedIn: true,
                },
            }
        case AuthActionType.SET_LOGOUT_STATE:
            localStorage.clear();
            return {
                ...state,
                auth: initAuth,
            }
        case AuthActionType.INITIALIZING:
            return {
                ...state,
                initialized: false,
            }
        case AuthActionType.INITIALIZED:
            return {
                ...state,
                initialized: true,
            }  
        default:
            return {
                ...state
            };
    }
}

export const AuthProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const verifyUser = async () => {
            dispatch({ type: AuthActionType.INITIALIZING });
            try {
                const token = localStorage.getItem('token');

                if (!token) {
                    console.log('logging out!');
                    dispatch({
                        type: AuthActionType.SET_LOGOUT_STATE,
                    });
                }

                const auth: Auth = await authAPI.getProfile(token as string);
                console.log(auth);
                dispatch({
                    type: AuthActionType.SET_LOGIN_STATE,
                    payload: auth,
                });
            } catch(error) {
                dispatch({
                    type: AuthActionType.SET_LOGOUT_STATE,
                });
            } finally {
                dispatch({ type: AuthActionType.INITIALIZED });
            }
        }

        verifyUser();
    }, [state.auth.loggedIn]);

    return (
        <AuthContext.Provider value={{state, dispatch}}>
            { children }
        </AuthContext.Provider>
    );
}
