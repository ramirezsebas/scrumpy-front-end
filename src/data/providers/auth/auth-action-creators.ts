import { Dispatch } from "react";
import authAPI from "../../../api/authScrumpyAPI";
import usuarioScrumpyAPI from "../../../api/usuarioScrumpyAPI";
import { Action } from "../../../shared/interfaces/action.interface";
import { Auth } from "../../../shared/interfaces/auth.interface";
import { UserStory } from "../../../shared/interfaces/user_story.interface";
import { AuthActionType } from "./auth-action-types";

export const doLogin = (auth: Auth): Action<Auth> => ({
    type: AuthActionType.SET_LOGIN_STATE,
    payload: auth,
});

export const doLogout = (): Action<Auth> => ({
    type: AuthActionType.SET_LOGOUT_STATE,
});

export const login = async (dispatch: Dispatch<Action<Auth>>, token_id: string) => {
        dispatch({ type: AuthActionType.INITIALIZING });
        const auth: Auth = await authAPI.login(token_id);
        dispatch(doLogin(auth));
        dispatch({ type: AuthActionType.INITIALIZED });
}
