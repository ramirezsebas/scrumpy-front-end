import axios, { AxiosInstance } from 'axios';
import { Permiso } from '../shared/interfaces/permiso.interface';
import { ProjectRole } from '../shared/interfaces/projectRole.interface';
import { serverURL } from '../config';

class ProjectRolScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL,
        });
    }

    private _baseProjectRole(id: number, idRole?: number) {
        return !idRole ? `/projectModule/projects/${id}/roles/` : `/projectModule/projects/${id}/roles/${idRole}/`;
    }

    async getRoles(id_proyecto: number): Promise<ProjectRole[]>{
        const token = localStorage.getItem("token")
        let response = await this.api.get(this._baseProjectRole(id_proyecto), {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async postRol(id_proyecto: number, rol: ProjectRole): Promise<ProjectRole>{
        const token = localStorage.getItem("token")
        let response = await this.api.post(this._baseProjectRole(id_proyecto), {...rol}, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async getPermisos(): Promise<Permiso[]>{
        const token = localStorage.getItem("token")
        //Nose si se usa aca
        let response = await this.api.get('/projectModule/permission/', {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async putRol(id_proyecto: number, rol: ProjectRole): Promise<ProjectRole> {
        const token = localStorage.getItem("token")
        let response = await this.api.patch(this._baseProjectRole(id_proyecto, rol.id), {...rol}, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async deleteRol(id_proyecto: number, rol: ProjectRole): Promise<ProjectRole> {
        const token = localStorage.getItem("token")
        let response = await this.api.delete(this._baseProjectRole(id_proyecto, rol.id), {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }
}

export default new ProjectRolScrumpyAPI();