import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { EventUS } from '../shared/interfaces/eventUS.interface';

class EventUSAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({});
    }

    async getAllEvents(id_project: number, id_us: number): Promise<EventUS[]> {
        this._setToken();
        let response = await this.api.get(`/sprintModule/projects/${id_project}/user_stories/${id_us}/history/`);
        return response.data;
    }


    _setToken() {
        const token = localStorage.getItem('token');
        this.api = axios.create({
            baseURL: serverURL,
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
    }
}

export default new EventUSAPI();