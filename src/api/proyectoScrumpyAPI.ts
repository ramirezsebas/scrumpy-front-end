import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { Proyecto } from '../shared/interfaces/proyecto.interface';
import { UserStory } from '../shared/interfaces/user_story.interface';

class ProyectoScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL
        });
    }

    private _baseProject(id?: string | number) {
        return id ? `/projectModule/projects/${id}/` : `/projectModule/projects/`;
    }

    async reeplazarUsuario(idProject: number, oldUserId: number, newUserId: number): Promise<UserStory> {
        const token = localStorage.getItem('token');
        let response = await this.api.post(`/projectModule/projects/${idProject}/replace/`, {
            id_old_member: oldUserId,
            id_new_member: newUserId
        }, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async getProyectos(token: string): Promise<Proyecto[]> {
        let response = await this.api.get(this._baseProject(), {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async getProyecto(id: number): Promise<Proyecto | null> {
        return this.api.get(this._baseProject(id));
    }

    async postProyecto(Proyecto: Proyecto): Promise<Proyecto> {
        const token = localStorage.getItem('token');
        let response = await this.api.post(this._baseProject(), { ...Proyecto }, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;   
    }

    async updateProyecto(proyecto: Proyecto): Promise<Proyecto> {
        const token = localStorage.getItem('token');
        let response = await this.api.patch(this._baseProject(proyecto.id), { ...proyecto }, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }
}

export default new ProyectoScrumpyAPI();