import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { UserStory } from '../shared/interfaces/user_story.interface';

class UserStoryScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL + "/sprintModule/projects/",
        });
    }

    async getUserStories(token: string, idProject: number | string): Promise<UserStory[]> {
        let response = await this.api.get(`${idProject}/user_stories/`, {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        return response.data;
    }

    async getUserStory(idProject: number, idUs: number): Promise<UserStory[]> {
        const token = localStorage.getItem('token');
        let response = await this.api.get(`${idProject}/user_stories/${idUs}/`, {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        return response.data;
    }

    /**
    * Se debe pasar:
    * {
    *   "name": "Funcionalidad de PIN",
    *   "description": "Solicitar un PIN al usuario cada vez que ingresa a la aplicación",
    *   "priority": 8
    * }
    * 
    */
    async postUserStory(idProject: number, userStory: Partial<UserStory>): Promise<UserStory> {
        const token = localStorage.getItem('token');
        let response = await this.api.post(`${idProject}/user_stories/`, { ...userStory }, {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        console.log(response.data);
        return response.data;
    }

    /**
    * Se debe pasar:
    *  {
    *   "name": "Funcionalidad de PIN",
    *   "description": "Solicitar un PIN al usuario cada vez que ingresa a la aplicación",
    *   "priority": 8
    *  }
    */

    async putUserStory(idProject: number, userStory: Partial<UserStory>): Promise<UserStory> {
        const token = localStorage.getItem('token');
        console.log('Body del patch: ' + JSON.stringify(userStory));
        let response = await this.api.patch(`${idProject}/user_stories/${userStory.id}/`, { ...userStory }, {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        return response.data;
    }

    // sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/backlog/
    async moverUserStoryASB(idProject: number, idSprint: number, idUS: number): Promise<UserStory> {
        const token = localStorage.getItem('token');
        let response = await this.api.post(`${idProject}/sprints/${idSprint}/backlog/`, {
            id_us: idUS,
        }, {
            headers: {
                'Authorization': `JWT ${token}`,
            }
        });
        return response.data;
    }

    async moverUserStoryAPB(idProject: number, idSprint: number, idUS: number): Promise<UserStory> {
        const token = localStorage.getItem('token');
        let response = await this.api.delete(`${idProject}/sprints/${idSprint}/backlog/${idUS}/`, {
            headers: {
                'Authorization': `JWT ${token}`,
            }
        });
        return response.data;
    }

    async reeplazarUsuario(idProject: number, oldUserId: number, newUserId: number): Promise<UserStory> {
        const token = localStorage.getItem('token');
        let response = await this.api.post(`${idProject}/replace/`, {
            id_old_member: oldUserId,
            id_new_member: newUserId
        }, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }
}

export default new UserStoryScrumpyAPI();