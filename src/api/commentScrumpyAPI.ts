import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { Comment } from "../shared/interfaces/comment.interface";
import { CommentRaw } from "../shared/interfaces/comment-raw.interface";

class CommentScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({});
    }

    async postComment(id_project: number, id_us: number, comment: CommentRaw): Promise<Comment> {
        this._setToken();
        let response = await this.api.post(`/sprintModule/projects/${id_project}/user_stories/${id_us}/comments/`, {
            ...comment
        })
        return response.data;
    }
    async getAllComments(id_project: number, id_us: number): Promise<Comment[]> {
        this._setToken();
        let response = await this.api.get(`/sprintModule/projects/${id_project}/user_stories/${id_us}/comments/`);
        return response.data;
    }
    async getComment(id_project: number, id_us: number, id_comment: number,): Promise<Comment> {
        this._setToken();
        let response = await this.api.get(`/sprintModule/projects/${id_project}/user_stories/${id_us}/comments/${id_comment}/`);
        return response.data;
    }

    async putComment(id_project: number, id_us: number, comment: Partial<Comment>): Promise<Comment> {
        this._setToken();
        let response = await this.api.put(`/sprintModule/projects/${id_project}/user_stories/${id_us}/comments/${comment.id}/`, {
            ...comment
        })
        return response.data;
    }

    async deleteComment(id_project: number, id_us: number, id_comment: number,): Promise<Comment> {
        this._setToken();
        let response = await this.api.delete(`/sprintModule/projects/${id_project}/user_stories/${id_us}/comments/${id_comment}/`);
        return response.data;
    }

    _setToken() {
        const token = localStorage.getItem('token');
        this.api = axios.create({
            baseURL: serverURL,
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
    }
}

export default new CommentScrumpyAPI();