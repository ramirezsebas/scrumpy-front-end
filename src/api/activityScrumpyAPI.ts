import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { Activity } from "../shared/interfaces/activity.interface";

class ActivityScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({});
    }

    async postActivity(id_project: number, id_us: number, activity: Partial<Activity>): Promise<Activity> {
        this._setToken();
        console.log(activity)
        let response = await this.api.post(`/sprintModule/projects/${id_project}/user_stories/${id_us}/activities/`, {
            description : activity.description,
            worked_hours : activity.worked_hours,
            created : activity.created
        })
        return response.data;
    }
    async getAllActivities(id_project: number, id_us: number): Promise<Activity[]> {
        this._setToken();
        let response = await this.api.get(`/sprintModule/projects/${id_project}/user_stories/${id_us}/activities/`);
        return response.data;
    }
    async getActivity(id_project: number, id_us: number, id_activity: number,): Promise<Activity> {
        this._setToken();
        let response = await this.api.get(`/sprintModule/projects/${id_project}/user_stories/${id_us}/activities/${id_activity}/`);
        return response.data;
    }

    async putActivity(id_project: number, id_us: number, activity: Partial<Activity>): Promise<Activity> {
        this._setToken();
        let response = await this.api.patch(`/sprintModule/projects/${id_project}/user_stories/${id_us}/activities/${activity.id}/`, {
            description : activity.description,
            worked_hours : activity.worked_hours,
            created : activity.created
        })
        return response.data;
    }


    _setToken() {
        const token = localStorage.getItem('token');
        this.api = axios.create({
            baseURL: serverURL,
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
    }
}

export default new ActivityScrumpyAPI();