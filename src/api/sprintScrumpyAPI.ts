import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { Activity } from '../shared/interfaces/activity.interface';
import { PastSprints } from '../shared/interfaces/past-sprints.interface';
import { Sprint } from '../shared/interfaces/sprint.interface';

class SprintScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL
        });
    }

    private _baseSprint(id: number, ids?: number) {
        return ids ? `/sprintModule/projects/${id}/sprints/${ids}/` : `/sprintModule/projects/${id}/sprints/`;
    }

    async getSprints(id_proyecto: number): Promise<Sprint[]> {
        const token = localStorage.getItem("token")
        let response = await this.api.get(this._baseSprint(id_proyecto), {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async postSprint(id_proyecto: number, sprint: Partial<Sprint>): Promise<Sprint> {
        const token = localStorage.getItem("token");
        console.log(sprint);
        console.log();
        console.log("asasasasa");
        let response = await this.api.post(this._baseSprint(id_proyecto), { ...sprint }, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        console.log("response");
        console.log(response);
        return response.data;   
    }

    async updateSprint(id_proyecto: number, sprint: Sprint, id_sprint: number): Promise<Sprint> {
        const token = localStorage.getItem("token");
        let response = await this.api.patch(this._baseSprint(id_proyecto, id_sprint), { ...sprint }, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async getPastSprintState(id_proyecto:number, id_sprint: number): Promise<PastSprints[]> {
        const token = localStorage.getItem("token");
        let response = await this.api.get(`sprintModule/projects/${id_proyecto}/past_sprints/${id_sprint}/`, {
            headers: {
                'Authorization': `JWT ${token}`,
            }
        });
        return response.data;
    }

    async getActivities(id_proyecto: number, id_sprint: number): Promise<Activity[]> {
        const token = localStorage.getItem("token");
        let response = await this.api.get(`sprintModule/projects/${id_proyecto}/sprints/${id_sprint}/activities/`, {
            headers: {
                'Authorization': `JWT ${token}`,
            }
        });
        return response.data;
    }
}

export default new SprintScrumpyAPI();