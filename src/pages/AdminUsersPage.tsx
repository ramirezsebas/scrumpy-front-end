import { useContext, useState } from 'react';
import { Box, Typography, Divider, Theme, makeStyles, createStyles, Fab } from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import ItemCardComponent from '../shared/components/ItemCardComponent';
import { Usuario, usuarioInitializer } from '../shared/interfaces/usuario.interface';
import UserFormComponent from '../shared/components/UserFormComponent';
import { UsuarioContext } from '../data/providers/usuario/usuario-context';
import { postUsuario, putUsuario } from '../data/providers/usuario/usuario-action-creators';
import PermissionsChecker from '../shared/components/PermissionsChecker';
import { PermissionCode } from '../data/constants/permisos';
import LoaderComponent from '../shared/components/LoaderComponent';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
        root: {
            display: 'flex',
          },
          content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
          },
    }),
);

export const AdminUsersPage = () => {
    const classes = useStyles();
    const [open, setOpen] = useState<string | false>(false);
    const [savedUser, setSavedUser] = useState<Usuario>(usuarioInitializer);
    const { state: { usuarios, loading }, dispatch } = useContext(UsuarioContext);

    const users: Usuario[] = usuarios;

    const handleSave = async (usuario: Usuario) => {
        await postUsuario(dispatch, usuario);
    }

    const handleUpdate = async (usuario: Usuario) => {
        await putUsuario(dispatch, usuario);
    }

    return (
        !loading ?
        <Box component="main" className={classes.content}>   
            <div>
                <Typography style={{ textAlign: 'left' }} variant="h2" >Usuarios</Typography>
            </div>
            <br />
            <Divider></Divider>
            <br />
            {users.map(usuario => (
                <ItemCardComponent
                    key={usuario.id}
                    upperLeft={usuario.first_name}
                    upperRight={usuario.is_active ? 'Activo' : 'Inactivo'}
                    lowerLeft={usuario.email}
                    marginBottom={10}
                    clickable
                    onClick={() => {
                        setSavedUser({
                            id: usuario.id,
                            email: usuario.email,
                            first_name: usuario.first_name,
                            last_name: usuario.last_name,
                            system_role: usuario.system_role,
                            is_active: usuario.is_active,
                            address: usuario.address,
                            phone_number: usuario.phone_number,
                            picture: usuario.picture,
                        });
                        setOpen('edit');
                    }}
                />
            ))}
            {
                <PermissionsChecker permissions={[PermissionCode.REGISTRAR_USUARIO,]}>
                    <Fab
                        color="primary"
                        className={classes.addButton}
                        onClick={() => setOpen('create')}
                    >
                        <AddIcon />
                    </Fab>
                </PermissionsChecker>
                
            }   
            <UserFormComponent isOpen={open === 'create'} handleClose={() => setOpen(false)} handleSave={handleSave} />
            <UserFormComponent isOpen={open === 'edit'} handleClose={() => setOpen(false)} savedUser={savedUser} handleUpdate={handleUpdate} />
        </Box> :
        <LoaderComponent />
    );
}

export default AdminUsersPage;
