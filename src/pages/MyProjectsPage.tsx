import { useContext, useState } from 'react';
import { Box, Typography, Divider, Theme, makeStyles, createStyles, Fab } from '@material-ui/core';
import CreateProjectFormComponent from '../shared/components/CreateProjectFormComponentModal';
import WithOutProjectComponent from '../shared/components/WithOutProjectComponent';
import WithProjectComponent from '../shared/components/WithProjectComponent';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { Proyecto } from '../shared/interfaces/proyecto.interface';
import { postProyecto } from '../data/providers/proyecto/proyecto-action-creators';
import { Add as AddIcon } from '@material-ui/icons';
import { AuthContext } from '../data/providers/auth/auth-context';
import PermissionsChecker from '../shared/components/PermissionsChecker';
import { PermissionCode } from '../data/constants/permisos';
import LoaderComponent from '../shared/components/LoaderComponent';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        myProyects: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-evenly'
        },
        showingProjectCards: {
            display: 'flex',
            flexWrap: 'wrap',
            width: '100%'
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
        },
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
    }),
);



export function MyProjectsPage() {
    const classes = useStyles();

    const [open, setOpen] = useState(false);


    const { state: { proyectos, loading }, dispatch } = useContext(ProyectoContext);
    const { state: { initialized } } = useContext(AuthContext);

    const projects: Proyecto[] = proyectos;

    const handleSave = async (proyecto: Proyecto) => {
        await postProyecto(dispatch, proyecto);
        handleClose();
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        !loading ?
        <Box component="main" className={classes.content}>
            <Box className={classes.myProyects}>
                <Box>
                    <Typography style={{ textAlign: 'left' }} variant="h2" >Mis Proyectos</Typography>
                </Box>
                <br />
                <Divider></Divider>
                <br />
                <Box className={classes.showingProjectCards}>
                    {projects.length > 0 ? 
                    <><WithProjectComponent open={open} handleOpen={handleOpen} handleSave={handleSave} handleClose={handleClose} projectList={projects} /> 
                        <PermissionsChecker permissions={[PermissionCode.CREAR_PROYECTO, ]}>
                            <Fab
                                color="primary"
                                className={classes.addButton}
                                onClick={handleOpen}
                            >
                                <AddIcon />
                            </Fab>
                        </PermissionsChecker>
                    </> : <WithOutProjectComponent handleOpen={handleOpen} />}
                </Box>
            </Box>
            <CreateProjectFormComponent open={open} handleOpen={handleOpen} handleSave={handleSave} handleClose={handleClose} />
        </Box>:
        <LoaderComponent />
        
    );
}
