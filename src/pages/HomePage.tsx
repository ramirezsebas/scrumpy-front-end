
import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import noData from "../assets/images/empty.svg";
import DrawerComponent from '../shared/components/SystemDrawerComponent';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
        content: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
        },
    }),
);

export default function HomePage() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <DrawerComponent />
            <main className={classes.content}>
                <Box textAlign="center">
                    <img src={noData} alt="" width="50%" />
                </Box>
            </main>
        </div>
    );
}
