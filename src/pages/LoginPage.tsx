import React, { Dispatch, FC, useContext, useEffect } from "react";
import { Box, Container, Grid, makeStyles, Typography } from "@material-ui/core"
import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login';
import imageData from "../assets/images/image-team.svg";
import { Action } from "../shared/interfaces/action.interface";
import DialogComponent from "../shared/components/DialogComponent";
import { clientId } from '../config';
import { AuthContext } from "../data/providers/auth/auth-context";
import { login } from "../data/providers/auth/auth-action-creators";
import { Auth } from "../shared/interfaces/auth.interface";
import { getResponseError } from "../shared/helpers/response-error";
import { AuthActionType } from "../data/providers/auth/auth-action-types";
import { ErrorContext } from "../data/providers/error/error-context";

interface GoogleResponse {
    tokenId: string;
}

const useStyle = makeStyles(() => ({
    root: {
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    loginButton: {
        backgroundColor: '#4285F4',
        padding: 15,
        border: 'none',
        borderRadius: 5,
        color: '#fff',
        fontSize: 16,
        cursor: 'pointer',
    }
}))

const LoginPage: FC = () => {
    const classes = useStyle();
    const { dispatch } = useContext(AuthContext);
    const { dialog, setDialog, handleClose } = useContext(ErrorContext);

    const responseGoogleOnSuccess = async (response: GoogleLoginResponse | GoogleLoginResponseOffline, dispatch: Dispatch<Action<Auth>>) => {
        let googleResponse = response as GoogleResponse;
        try {
            console.log(googleResponse.tokenId);
            await login(dispatch, googleResponse.tokenId);
        } catch (error) {
            let responseError: any = getResponseError(error);
            console.log(responseError);
            setDialog({
                title: 'Atención ⚠️',
                body: responseError.message,
                buttonLabel: 'Aceptar',
                open: true
            });
            console.log('dialog fijado');
            dispatch({ type: AuthActionType.INITIALIZED });
        } finally {
            dispatch({ type: AuthActionType.INITIALIZED });
        }
    }

    useEffect(() => console.log(dialog), [dialog]);

    const responseGoogleOnFailure = (response: any) => {
        console.log("FAIL");
        console.log(response);
        console.log("++++++");
    }

    return (
        <Container className={classes.root}>
            <Grid container>
                <Grid item xs={12} md={6} >
                    <Box textAlign="center">
                        <img src={imageData} alt="" width="95%" />
                    </Box>
                </Grid>
                <Grid item xs={12} md={6} >
                    <Box textAlign="center" mt={5}>
                        <Typography variant="h4" >
                            Scrum  + Kanban = Sencillo 😎
                        </Typography>
                    </Box>
                    <Box textAlign="left" mt={5}>
                        <Typography variant="subtitle1" >
                            Con Scrumpy la gestión de tus proyectos agiles es simple y sencilla, esto es lo que tu equipo necesita para aumentar la productividad 🔥🔥🔥
                        </Typography>
                    </Box>
                    <Box textAlign="center" mt={5}>
                        <GoogleLogin
                            clientId={ clientId }
                            buttonText="Iniciar sesión con Google"
                            onSuccess={async (response) => await responseGoogleOnSuccess(response, dispatch)}
                            onFailure={responseGoogleOnFailure}
                            className={classes.loginButton}
                            render={renderProps => (
                                <button onClick={renderProps.onClick} className={classes.loginButton}>Iniciar sesión con Google</button>)
                            }
                        />
                    </Box>
                </Grid>
            </Grid>
            <DialogComponent dialog={dialog} handleClose={handleClose} />
        </Container >
    )
}

export default LoginPage;