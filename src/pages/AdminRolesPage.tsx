import { useContext, useState } from 'react'
import { Box, Typography, Divider, Theme, makeStyles, createStyles, Fab } from '@material-ui/core';
import { RolContext } from '../data/providers/rol/rol-context';
import { Add as AddIcon } from '@material-ui/icons';
import ItemCardComponent from '../shared/components/ItemCardComponent';
import { Role, roleInitializer } from '../shared/interfaces/role.interface';
import RolFormComponent from '../shared/components/RolFormComponent';
import { deleteRol, postRol, putRol, setLoaded } from '../data/providers/rol/rol-actions-creators';
import PermissionsChecker from '../shared/components/PermissionsChecker';
import { PermissionCode } from '../data/constants/permisos';
import LoaderComponent from '../shared/components/LoaderComponent';
import { getResponseError } from '../shared/helpers/response-error';
import DialogComponent from '../shared/components/DialogComponent';
import { ErrorContext } from '../data/providers/error/error-context';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
        container: {
            display: 'flex',
            flexGrow: 1,
            justifyContent: 'center',
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
            height: '100vh',
        },
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
    }),
);

function AdminRolesPage() {
    const classes = useStyles();
    const [open, setOpen] = useState<string | false>(false);
    const [savedRol, setSavedRol] = useState<Role>(roleInitializer);
    const { state: { roles, loading } } = useContext(RolContext);
    const { dialog, setDialog, handleClose } = useContext(ErrorContext);

    const { dispatch } = useContext(RolContext);

    const handleSave = async (rol: Role) => {
        await postRol(dispatch, rol);
    }

    const handleUpdate = async (rol: Role) => {
        await putRol(dispatch, rol);
    }

    const handleDelete = async (rol: Role) => {
        try {
            console.log(rol);
            await deleteRol(dispatch, rol);
        } catch (error) {
            let responseError: any = getResponseError(error);
            console.log(responseError);
            setDialog({
                title: 'Atención ⚠️',
                body: responseError.message,
                buttonLabel: 'Aceptar',
                open: true
            });
            console.log('dialog fijado');
            dispatch(setLoaded());
        } finally {
            dispatch(setLoaded());
        }
    }

    return (
        !loading ?
            <Box component="main" className={classes.content}>
                <div>
                    <Typography style={{ textAlign: 'left' }} variant="h2" >Roles</Typography>
                </div>
                <br />
                <Divider></Divider>
                <br />
                {roles.map(rol => (
                    <ItemCardComponent
                        key={rol.id}
                        upperLeft={rol.name}
                        lowerLeft={rol.description}
                        marginBottom={10}
                        clickable
                        onClick={() => {
                            setSavedRol({
                                id: rol.id,
                                description: rol.description,
                                name: rol.name,
                                permissions: rol.permissions,
                            });
                            setOpen('edit');
                        }}
                    />
                ))}
                <PermissionsChecker permissions={[PermissionCode.CREAR_ROL,]}>
                    <Fab
                        color="primary"
                        className={classes.addButton}
                        onClick={() => setOpen('create')}
                    >
                        <AddIcon />
                    </Fab>
                </PermissionsChecker>
                <RolFormComponent handleSave={handleSave} open={open === 'create'} cerrarModal={() => setOpen(false)} />
                <RolFormComponent open={open === 'edit'} cerrarModal={() => setOpen(false)} savedRol={savedRol} handleUpdate={handleUpdate} handleDelete={handleDelete} />
                <DialogComponent dialog={dialog} handleClose={handleClose} />
            </Box> :
            <LoaderComponent />
    )
}

export default AdminRolesPage
