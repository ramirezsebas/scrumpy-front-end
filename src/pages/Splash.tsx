import { useContext, FC } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LoaderComponent from '../shared/components/LoaderComponent';
import PrivateRouter from '../shared/components/PrivateRouterComponent';
import LoginPage from './LoginPage';
import AdminRolesPage from './AdminRolesPage';
import AdminUsersPage from './AdminUsersPage';
import { MyProjectsPage } from './MyProjectsPage';
import { TeamPage } from './TeamPage';
import PlanningPage from './PlanningPage';
import { AuthContext } from '../data/providers/auth/auth-context';
import { Container } from '@material-ui/core';
import SystemDrawerComponent from '../shared/components/SystemDrawerComponent';
import MyDesktopPage from './MyDesktopPage';
import SprintPage from './SprintPage';
import ConfiguracionesPage from './ConfiguracionesPage';
import ReportesPage from './ReportesPage';
import AdminProjectRolesPage from './AdminProjectRolesPage';
import { Navbar } from '../shared/components/Navbar';



const Splash: FC = () => {
    /**
     * initialized: nos indica si la petición ya finalizó o no
     * loggedIn: nos indica si el user está o no logueado
     */
    const { state: { initialized, auth: { loggedIn, usuario } } } = useContext(AuthContext);

    return (
        <Router>
            <Container component="div" style={{ display: 'flex', height: '100%' }} maxWidth="lg" >
                {loggedIn && <SystemDrawerComponent />}
                <Switch>

                    {
                        /**
                         * Si aun no finalizó la petición, se muestra el loader
                         */
                        !initialized && <LoaderComponent />
                    }
                    {
                        /**
                         * Si la petición la finalizó y el user no se encuentra logueado, se muestra la página de login
                         */
                        initialized && !loggedIn && <Route exact path="/" component={LoginPage} />
                    }
                    {
                        /**
                         * Si la petición la finalizó y el user se encuentra logueado, se muestra la página correspondiente 
                         */
                        <div style={{ display: 'flex', flexDirection: 'column',width:'100%' }}>
                            <Navbar usuario={usuario}  />
                            <div>
                                <PrivateRouter exact path="/" component={MyDesktopPage} />
                                <PrivateRouter exact path="/proyectos" component={MyProjectsPage} />
                                <PrivateRouter exact path="/projectDetail/:id/planning" component={PlanningPage} />
                                <PrivateRouter exact path="/projectDetail/:id/sprint" component={SprintPage} />
                                <PrivateRouter exact path="/projectDetail/:id/equipo" component={TeamPage} />
                                <PrivateRouter exact path="/projectDetail/:id/reportes" component={ReportesPage} />
                                <PrivateRouter exact path="/projectDetail/:id/configuraciones" component={ConfiguracionesPage} />
                                <PrivateRouter exact path="/admin-users" component={AdminUsersPage} />
                                <PrivateRouter exact path="/admin-roles" component={AdminRolesPage} />
                                <PrivateRouter exact path="/projectDetail/:id/projectRoles" component={AdminProjectRolesPage} />
                            </div>
                        </div>
                    }
                </Switch>
            </Container>
        </Router>

    );
}

export default Splash;