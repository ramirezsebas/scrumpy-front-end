import { useContext, useState } from 'react'
import { Box, Typography, Divider, Theme, makeStyles, createStyles, Fab } from '@material-ui/core';
import { ProjectRolContext } from '../data/providers/rolProyecto/rolProyecto-context';
import { Add as AddIcon } from '@material-ui/icons';
import ItemCardComponent from '../shared/components/ItemCardComponent';
import { ProjectRole, roleInitializer } from '../shared/interfaces/projectRole.interface';
import RolFormComponent from '../shared/components/ProjectRolFormComponent';
import { deleteRol, postRol, putRol } from '../data/providers/rolProyecto/rolProyecto-actions-creators';
import PermissionsChecker from '../shared/components/PermissionsChecker';
import { ProjectPermissionsCode } from '../data/constants/permisos';
import LoaderComponent from '../shared/components/LoaderComponent';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { ErrorContext } from '../data/providers/error/error-context';
import { getResponseError } from '../shared/helpers/response-error';
import { setLoaded } from '../data/providers/rol/rol-actions-creators';
import DialogComponent from '../shared/components/DialogComponent';


const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
        container: {
            display: 'flex',
            flexGrow: 1,
            justifyContent: 'center',
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
            height: '100vh',
        },
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
    }),
);

function AdminProjectRolesPage() {
    const classes = useStyles();
    const [open, setOpen] = useState<string | false>(false);
    const [savedRol, setSavedRol] = useState<ProjectRole>(roleInitializer);
    const { state: { roles, loading } } = useContext(ProjectRolContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { dispatch } = useContext(ProjectRolContext);
    const { dialog, setDialog, handleClose } = useContext(ErrorContext);

    const handleSave = async (rol: ProjectRole) => {

        await postRol(dispatch, rol, currentProject.id as number);
    }

    const handleUpdate = async (rol: ProjectRole) => {
        await putRol(dispatch, rol, currentProject.id as number);
    }

    const handleDelete = async (rol: ProjectRole) => {
        try {
            console.log(rol);
            await deleteRol(dispatch, rol, currentProject.id as number);
        } catch (error) {
            let responseError: any = getResponseError(error);
            console.log(responseError);
            setDialog({
                title: 'Atención ⚠️',
                body: responseError.message,
                buttonLabel: 'Aceptar',
                open: true
            });
            console.log('dialog fijado');
            dispatch(setLoaded());
        } finally {
            dispatch(setLoaded());
        }
    }

    return (
        !loading ?
            <Box component="main" className={classes.content}>
                <div>
                    <Typography style={{ textAlign: 'left' }} variant="h2" >Roles</Typography>
                </div>
                <br />
                <Divider></Divider>
                <br />
                {roles.map(rol => (
                    <ItemCardComponent
                        key={rol.id}
                        upperLeft={rol.name}
                        lowerLeft={rol.description}
                        marginBottom={10}
                        clickable
                        onClick={() => {
                            setSavedRol({
                                id: rol.id,
                                description: rol.description,
                                name: rol.name,
                                project: rol.project,
                                permissions: rol.permissions,
                            });
                            setOpen('edit');
                        }}
                    />
                ))}
                {currentProject.status !== "FINALIZADO" && <PermissionsChecker
                    tipo="Project"
                    permissions={[ProjectPermissionsCode.CREAR_ROL_DE_PROYECTO,]}
                >
                    <Fab
                        color="primary"
                        className={classes.addButton}
                        onClick={() => setOpen('create')}
                    >
                        <AddIcon />
                    </Fab>
                </PermissionsChecker>
                }
                <RolFormComponent handleSave={handleSave} open={open === 'create'} cerrarModal={() => setOpen(false)} />
                <RolFormComponent open={open === 'edit'} cerrarModal={() => setOpen(false)} savedRol={savedRol} handleUpdate={handleUpdate} handleDelete={handleDelete} />
                <DialogComponent dialog={dialog} handleClose={handleClose} />
            </Box> :
            <LoaderComponent />
    )
}

export default AdminProjectRolesPage
