import { Typography } from '@material-ui/core';
import dayjs from 'dayjs';
import React, { useContext, useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import sprintScrumpyAPI from '../api/sprintScrumpyAPI';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { SprintContext } from '../data/providers/sprint/sprint-context';
import { UserStoryContext } from '../data/providers/userstory/user-story-context';
import { Activity } from '../shared/interfaces/activity.interface';
import { UserStory } from '../shared/interfaces/user_story.interface';

const ReportesPage = () => {

    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentSprint } } = useContext(SprintContext);
    const { state: { userStories } } = useContext(UserStoryContext);
    const [remainingTaskByDay, setRemainingTaskByDay] = useState<number[]>([]);
    const [remainingEffortByDay, setRemainingEffortByDay] = useState<number[]>([]);
    const [activities, setActivities] = useState<Activity[]>([]);

    useEffect(() => {
      (async () => {
        let actividades = await sprintScrumpyAPI.getActivities(currentProject.id, currentSprint?.id);
        setActivities(actividades);
        console.log(actividades);
      })();
    }, []);

    useEffect(() => {
      getRemainingTasks();
      getRemainingEffort();
    }, [activities]);

    let daysToMs = (days: number) => days * 24 * 60 * 60 * 1000;

    const getSprintDay = () => {
        let startDate = dayjs(currentSprint?.startDay).toDate().getTime();

        const diffTime = Math.abs(Date.now() - startDate);

        const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));

        return diffDays + 1;
    }

    const getHorizontalAxis = (days: number) => {
        let axis = [];
        for(let index = 0; index <= days; index++) {
            axis.push(index);
        }
        return axis;
    }

    const isDateEqual = (a: Date, b: Date) => {
      console.log(a);
      console.log(b);
      return a.getDate() === b.getDate() && a.getMonth() === b.getMonth();
    }

    // Obtiene todos los US finalizados en la fecha indicada
    const getFinalizados = (fecha: Date) => {
      return userStories
        .filter(story => (story.sprint as any)?.id === currentSprint?.id && story.status === 'DONE')
        .filter(story => {
          return isDateEqual(dayjs(story.finish_date).toDate(), fecha)
        });
    }

    // Obtiene la suma de las estimaciones de los stories especificados
    const getSumaEstimaciones = (stories: UserStory[]) => {
      let suma: number = 0;

      stories.forEach(story => {
        suma += story.estimate;
      });

      return suma;
    }

    const getTotalEffort = () => {
      let sprintBacklog = userStories
              .filter(us => us.sprint !== null)
              .filter(us => (us.sprint as any).iteration === currentSprint?.iteration);
      
      let totalEffort = 0;

      sprintBacklog.forEach(story => totalEffort += story.estimate);

      return totalEffort;
    }

    const getRemainingTasks = () => {
        let index = currentProject.sprintDuration * 7 - getSprintDay() + 1;

        let prevData = [getTotalEffort(), ]

        for(let i = 1; i <= getSprintDay(); i++) {
          // Obtenemos la fecha correspondiente al i-esimo dia del sprint
          const fechaInicio = dayjs(currentSprint?.startDay).toDate();
          let fecha = new Date(fechaInicio);
          fecha.setDate(fecha.getDate() + i - 1);

          // Obtenemos todos los user stories finalizados en la fecha indicada
          let storiesFinalizados = getFinalizados(fecha);
          console.log(storiesFinalizados);

          // Calculamos las horas terminadas
          let horasTerminadas = getSumaEstimaciones(storiesFinalizados);
          console.log(horasTerminadas);

          let yesterdayRemainingEffort = prevData[i - 1];
          prevData.push(yesterdayRemainingEffort - horasTerminadas);
        }
        setRemainingTaskByDay([...prevData]);
    }

    const getSumaHorasActividades = (fecha: Date) => {
      const actividadesFecha = activities.filter(activity => isDateEqual(dayjs(activity.created).toDate(), fecha));
      console.log(actividadesFecha);
      return actividadesFecha
        .map(activity => activity.worked_hours)
        .reduce((suma: number, horas: number) => suma + horas, 0);
    }

    const getRemainingEffort = () => {
      let prevData = [getTotalEffort(), ]

      for(let i = 1; i <= getSprintDay(); i++) {
        // Obtenemos la fecha correspondiente al i-esimo dia del sprint
        const fechaInicio = dayjs(currentSprint?.startDay).toDate();
        let fecha = new Date(fechaInicio);
        fecha.setDate(fecha.getDate() + i - 1);

        // Calculamos el total de horas cargadas en actividades en el i-esimo dia del sprint
        let horasTrabajadas = getSumaHorasActividades(fecha);
        console.log(horasTrabajadas)
        prevData.push(prevData[i-1] - horasTrabajadas);
      }

      setRemainingEffortByDay([...prevData]);
    }
    
    const getEquationSolver = (a: number, b:number) => {
        let m = (b / a) * (-1);
        return (x: number) => {
            return m * x + b;
        }
    }

    const sprintDuration = currentProject.sprintDuration * 7;
    const totalEffort = getTotalEffort();
    const getY = getEquationSolver(sprintDuration, totalEffort);
    
    const data = {
      labels: getHorizontalAxis(sprintDuration),
      datasets: [
        {
          label: 'Ideal',
          data: getHorizontalAxis(sprintDuration).map(x => getY(x)),
          fill: false,
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgba(255, 99, 132)',
        },
        {
          label: 'Remaining tasks',
          data: remainingTaskByDay,
          fill: false,
          backgroundColor: 'rgb(0, 0, 255)',
          borderColor: 'rgba(0, 0, 255, 0.4)',
        },
        {
          label: 'Remaining effort',
          data: remainingEffortByDay,
          fill: false,
          backgroundColor: 'rgb(3, 160, 98)',
          borderColor: 'rgba(23, 160, 98, 0.4)',
        },
      ],
    };
    
    const options = {
      scales: {
        y: {
          beginAtZero: true,
        }
      }
    };

    return (
        <div>
            <Typography variant="h2" style={{marginBottom: 50}}>Burndown Chart</Typography>
            <Line data={data} options={options} />
        </div>
    )
}

export default ReportesPage
