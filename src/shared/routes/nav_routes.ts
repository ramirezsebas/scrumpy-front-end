import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import { Person, Lock, Toc, TableChart, InsertChart, People, Settings } from '@material-ui/icons';
import { PermissionCode } from '../../data/constants/permisos';


export const actionList = [
    {
        title: "Mi escritorio",
        icon: DesktopWindowsIcon,
        link: '/',
        permisos: [],
    },
    {
        title: "Proyectos",
        icon: EmojiObjectsIcon,
        link: '/proyectos',
        permisos: [],
    },
    {
        title: "Usuarios",
        icon: Person,
        link: '/admin-users',
        permisos: [
            PermissionCode.REGISTRAR_USUARIO,
            PermissionCode.MODIFICAR_USUARIO,
        ],
    },
    {
        title: "Roles",
        icon: Lock,
        link: '/admin-roles',
        permisos: [
            PermissionCode.CREAR_ROL,
            PermissionCode.MODIFICAR_ROL,
            PermissionCode.ELIMINAR_ROL,
        ],
    },

];

export const projectActionList = [
    {
        title: "Planning",
        icon: Toc,
        link: '/planning',
        permisos: [],
    },
    {
        title: "Sprint",
        icon: TableChart,
        link: '/sprint',
        permisos: [],
    },
    {
        title: "Reportes",
        icon: InsertChart,
        link: '/reportes',
        permisos: [],
    },
    {
        title: "Equipo",
        icon: People,
        link: '/equipo',
        permisos: [],
    },
    {
        title: "Roles",
        icon: Lock,
        link: '/projectRoles',
        permisos: [],
    },
    {
        title: "Configuraciones",
        icon: Settings,
        link: '/configuraciones',
        permisos: [],
    },
];