import axios, { AxiosError } from 'axios';

export const getResponseError = (error: any) => {
    if (axios.isAxiosError(error)) {
        error = error as AxiosError;
        if (error.response?.data) {
            return ({
                code: error.response?.data.code || "Code unknown",
                message: error.response?.data.message || "Message unknown",
            })
        } else {
            return ({
                code: "00",
                message: "No se ha podido comunicar con el servidor, por favor compruebe su acceso a internet y vuelva intentarlo. Si el problema persiste contacte a soporte.",
            })
        }
    }
};