import React, { FC, useCallback, useContext, useState } from 'react';
import { Avatar, Card, CardContent, CardHeader, Divider, IconButton, TextField, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Activity, ActivityInitializer } from '../interfaces/activity.interface';
//import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { ActivityContext } from '../../data/providers/activity/activity-context';
import { AuthContext } from '../../data/providers/auth/auth-context';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { postActivity, putActivity } from '../../data/providers/activity/activity-action-creators';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import ActivityFormComponent from './ActivityFormComponent';
import authScrumpyAPI from '../../api/authScrumpyAPI';

type ActivityProps = {
    activity: Activity;
    clickable?: boolean;
    onClick?: () => void;
}

const ActivityCardComponent: FC<ActivityProps> = ({ activity, onClick}) => {
    const { state: { auth: { usuario }, } } = useContext(AuthContext);
    const { state: { miembros } } = useContext(MiembroContext);
    const [open, setOpen] = useState(false);
    const [savedActivity, setSavedActivity] = useState<Activity>(ActivityInitializer);
    const { state: { activities, loading } } = useContext(ActivityContext);
    const [editing, setEditing] = useState(false);
    const { dispatch } = useContext(ActivityContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);
    const [open2, setOpen2] = useState<string | false>(false);
    const { state: { auth } } = useContext(AuthContext);


    const updateActivity = async (activity: Partial<Activity>) => {
        await putActivity(dispatch, currentProject.id, currentUS.id, activity);
    }

    const getMiembroById = useCallback((id: number) => {
		return miembros.find(miembro => miembro.id === id);
	}, [miembros]);

    const miembroVsUser = () => {
		return getMiembroById(currentUS.member)?.user.id === auth.usuario.id;
	}

    return (
        <Card style={{ marginBottom: 10, position: 'relative' }} onClick={onClick}>
            <CardHeader
                avatar={
                    <>
                        {
                            (activity?.user?.picture?.length || 0) > 0 &&
                            <Avatar aria-label="recipe" src={activity?.user?.picture} />
                        }
                        {
                            activity?.user?.picture?.length === 0 &&
                            <Avatar aria-label="recipe">
                                {(activity?.user?.first_name[0] || '') + (activity?.user?.last_name[0] || '')}
                            </Avatar>
                        }
                    </>
                }

                action={
                    currentProject.status !== "FINALIZADO" && miembroVsUser() && currentUS.status === 'DOING' &&
                        <IconButton aria-label="settings" onClick={() => setOpen(!open)}>
                            <MoreVertIcon />
                        </IconButton>
                }
                title={((currentUS.member as any)?.user as any)?.email}
                subheader={activity.created.substr(0, 10)}
            />
            <CardContent>
                        
                <Typography>
                    {activity.description}
                </Typography>
                <Typography>

                    Tiempo dedicado: {activity.worked_hours} hs
                </Typography>
                
            </CardContent>
            <Card style={{
                position: 'absolute',
                top: '20%',
                right: '15%',
                backgroundColor: '#fff',
                padding: 10,
                display: open ? 'block' : 'none'
            }}
                onBlur={() => setOpen(false)}
            >
                {
                    currentProject.status !== "FINALIZADO" && miembroVsUser() && currentUS.status === 'DOING' &&
                    <Typography
                        style={{ cursor: 'pointer' }}
                        onClick={() => {
                            setSavedActivity({
                                id: activity.id,
                                sprint: activity.sprint,
                                user_story: activity.user_story,
                                user: activity.user,
                                description: activity.description,
                                worked_hours: activity.worked_hours,
                                created: activity.created,
                            });
                            setEditing(true);
                            setOpen(false);
                            {setOpen2('edit');}
                        }}
                    >
                        Editar
                    </Typography>
                }
                <Divider style={{ margin: 5 }} />
            </Card>
            <ActivityFormComponent handleUpdate={updateActivity} open={open2 === 'edit'} cerrarModal={() => setOpen2(false)} savedActivity={savedActivity} />
        </Card>
    )
}

export default ActivityCardComponent