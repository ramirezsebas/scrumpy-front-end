import { AppBar, Avatar, Toolbar, Typography, Box, makeStyles } from '@material-ui/core';
import React, { FC, useContext, useEffect, useState } from 'react'
import { useLocation, useRouteMatch } from 'react-router';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { Usuario } from '../interfaces/usuario.interface';


const useStyles = makeStyles(theme => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  navBar: {
    marginBottom: '10px'
  }
}));

type NavbarProps = {
  usuario: Usuario | null
}
export const Navbar: FC<NavbarProps> = ({ usuario }) => {
  const classes = useStyles();
  const { state: { currentProject } } = useContext(ProyectoContext);

  let match = useLocation();

  let upperName = (usuario && usuario.first_name && usuario?.first_name.replace(usuario?.first_name[0], usuario?.first_name[0].toUpperCase()) || "");
  let upperLastName = (usuario && usuario.last_name && usuario?.last_name.replace(usuario?.last_name[0], usuario?.last_name[0].toUpperCase()) || "");
  return (

    <AppBar className={classes.navBar} position="sticky">
      <Toolbar>
        <Typography className={classes.title} variant="h6">{upperName} {upperLastName}</Typography>
        {match.pathname.includes('projectDetail') && <Typography className={classes.title} variant="h6">Proyecto {currentProject.status === "FINALIZADO" ? "Finalizado":"Actual"}: {currentProject.name}</Typography>}
        {match.pathname.includes('projectDetail') && currentProject.status !== "FINALIZADO" && <Typography className={classes.title} variant="h6">Deadline: {currentProject.endDay}</Typography>}
        <Avatar className={classes.menuButton} alt="" src={usuario?.picture ? usuario.picture : ""} />
      </Toolbar>
    </AppBar>
  );



}
