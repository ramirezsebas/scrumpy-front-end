import React, { FC, useCallback, useContext, useState } from 'react';
import { Avatar, Card, CardContent, CardHeader, Divider, IconButton, TextField, Typography } from '@material-ui/core';
import { AuthContext } from '../../data/providers/auth/auth-context';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import authScrumpyAPI from '../../api/authScrumpyAPI';
import { EventUS, EventUSInitializer } from '../interfaces/eventUS.interface';
import { EventUSContext } from '../../data/providers/eventUS/eventUS-context';

type EventUSProps = {
    event: EventUS;
    clickable?: boolean;
    onClick?: () => void;
}

const EventUSCardComponent: FC<EventUSProps> = ({ event, onClick}) => {
    const { state: { auth: { usuario }, } } = useContext(AuthContext);
    const { state: { miembros } } = useContext(MiembroContext);
    const [open, setOpen] = useState(false);
    const [savedEventUS, setSavedEventUS] = useState<EventUS>(EventUSInitializer);
    const { state: { events, loading } } = useContext(EventUSContext);
    const [editing, setEditing] = useState(false);
    const { dispatch } = useContext(EventUSContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);
    const [open2, setOpen2] = useState<string | false>(false);
    const { state: { auth } } = useContext(AuthContext);


    return (
        <Card style={{ marginBottom: 10, position: 'relative' }} onClick={onClick}>
            <CardHeader
                avatar={
                    <>
                        {
                            (event?.user?.picture?.length || 0) > 0 &&
                            <Avatar aria-label="recipe" src={event?.user?.picture} />
                        }
                        {
                            event?.user?.picture?.length === 0 &&
                            <Avatar aria-label="recipe">
                                {(event?.user?.first_name[0] || '') + (event?.user?.last_name[0] || '')}
                            </Avatar>
                        }
                    </>
                }

                title={event?.user?.email}
                subheader={event.date.substr(0, 10)}
            />
            <CardContent>
                        
                <Typography>
                    {event.description}
                </Typography>
                
            </CardContent>
            <Card style={{
                position: 'absolute',
                top: '20%',
                right: '15%',
                backgroundColor: '#fff',
                padding: 10,
                display: open ? 'block' : 'none'
            }}
                onBlur={() => setOpen(false)}
            >

                <Divider style={{ margin: 5 }} />
            </Card>
        </Card>
    )
}

export default EventUSCardComponent