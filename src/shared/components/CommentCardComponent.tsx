import React, { FC, useContext, useState } from 'react';
import { Avatar, Card, CardContent, CardHeader, Divider, IconButton, TextField, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Comment } from '../interfaces/comment.interface';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { CommentContext } from '../../data/providers/comment/comment-context';
import { deleteComment } from '../../data/providers/comment/comment-action-creators';

type CommentProps = {
    comment: Comment;
    handleUpdate: (comment: Partial<Comment>) => Promise<void>;
    readOnly?: boolean;
}

const CommentCardComponent: FC<CommentProps> = ({ comment, handleUpdate, readOnly = false }) => {
    const [open, setOpen] = useState(false);
    const [editing, setEditing] = useState(false);
    const [content, setContent] = useState(comment.content);
    const { state: { currentMember } } = useContext(MiembroContext);
    const { dispatch } = useContext(CommentContext);

    const handleDelete = async () => {
        await deleteComment(dispatch, comment);
    }

    return (
        <Card style={{ marginBottom: 10, position: 'relative' }}>
            <CardHeader
                avatar={
                    <>
                        {
                            (comment.member.user.picture?.length || 0) > 0 &&
                            <Avatar aria-label="recipe" src={comment.member.user.picture} />
                        }
                        {
                            comment.member.user.picture?.length === 0 &&
                            <Avatar aria-label="recipe">
                                {comment.member.user.first_name[0] + comment.member.user.last_name[0]}
                            </Avatar>
                        }
                    </>
                }
                action={
                    currentMember.id === comment.member.id ?
                        <IconButton aria-label="settings" onClick={() => !readOnly && setOpen(!open)}>
                            <MoreVertIcon />
                        </IconButton> :
                        null
                }
                title={comment.member.user.email}
                subheader={comment.created.substr(0, 10)}
            />
            <CardContent>
                {
                    editing ?
                        <TextField
                            fullWidth
                            autoFocus
                            value={content}
                            onChange={event => setContent(event.target.value)}
                            margin="dense"
                            multiline
                            rows={4}
                            id="story-name"
                            variant="outlined"
                            onBlur={async () => {
                                setEditing(false);
                                await handleUpdate({ id: comment.id, content });
                            }}
                        /> :
                        <Typography>
                            {comment.content}
                        </Typography>
                }
            </CardContent>
            <Card style={{
                position: 'absolute',
                top: '20%',
                right: '15%',
                backgroundColor: '#fff',
                padding: 10,
                display: open ? 'block' : 'none'
            }}
                onBlur={() => setOpen(false)}
            >
                <Typography 
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                        setEditing(true);
                        setOpen(false);
                    }}
                >
                    Editar
                </Typography>
                <Divider style={{ margin: 5 }} />
                <Typography
                    style={{ cursor: 'pointer' }}
                    onClick={handleDelete}
                >
                    Eliminar
                </Typography>
            </Card>
        </Card>
    )
}

export default CommentCardComponent
