import { Drawer, Box, Typography, Divider, List, ListItem, ListItemIcon, ListItemText, createStyles, makeStyles, Theme } from '@material-ui/core'
import React, { useContext, FC } from 'react'
import { Link } from 'react-router-dom'
import { actionList } from '../routes/nav_routes';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { AuthContext } from '../../data/providers/auth/auth-context';
import { doLogout } from '../../data/providers/auth/auth-action-creators';

const drawerWidth = 240;


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
    }),
);

const ProjectDrawerComponent: FC = () => {
    const classes = useStyles();
    const { dispatch } = useContext(AuthContext);
    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
            anchor="left"
        >
            <Box textAlign="center" mt={5} mb={5}>
                <Typography>
                    SCRUMPY
                </Typography>
            </Box>
            <Divider />
            <List>
                {actionList.map((item, index) => (
                    <Link key={item.title} to={item.link} style={{textDecoration: 'none', color: "#302929"}}>
                        <ListItem button >
                            <ListItemIcon>{<item.icon />}</ListItemIcon>
                            <ListItemText primary={item.title} />
                        </ListItem>
                    </Link>
                ))}
            </List>
            <Divider />
            <List>
                <ListItem button onClick={() => dispatch(doLogout())}>
                    <ListItemIcon><ExitToAppIcon /></ListItemIcon>
                    <ListItemText>Logout</ListItemText>
                </ListItem>
            </List>
        </Drawer>
    )
}

export default ProjectDrawerComponent;
