import React, { FC, useContext } from 'react';
import {
  Document,
  Page,
  View,
  Text,
  Link,
  Font,
  StyleSheet,
} from '@react-pdf/renderer';
import { UserStory } from '../interfaces/user_story.interface';

const styles = StyleSheet.create({
  title: {
    margin: 20,
    fontSize: 25,
    textAlign: 'center',
    backgroundColor: '#e4e4e4',
    textTransform: 'uppercase',
    fontFamily: 'Oswald',
  },
  body: {
    flexGrow: 1,
    marginHorizontal: 30,
  },
  row: {
    flexGrow: 1,
    flexDirection: 'row',
  },
  block: {
    flexGrow: 1,
  },
  text: {
    width: '60%',
    margin: 10,
    fontFamily: 'Oswald',
    textAlign: 'justify',
  },
  fill1: {
    width: '40%',
    backgroundColor: '#e14427',
  },
  fill2: {
    flexGrow: 2,
    backgroundColor: '#e6672d',
  },
  fill3: {
    flexGrow: 2,
    backgroundColor: '#e78632',
  },
  fill4: {
    flexGrow: 2,
    backgroundColor: '#e29e37',
  },
});

Font.register({
  family: 'Oswald',
  src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf',
});

type PdfProps = {
  stories: UserStory[];
}

const ReporteSprintComponent: FC<PdfProps> = ({ stories }) => {
  return (
    <Document>
      <Page size="A4">
        <Text
          style={styles.title}
        >
          Sprint Backlog
        </Text>
        <View style={styles.body}>
          {
            stories.map(story => (
              <View key={story.id} style={{marginBottom: 15}}>
                <Text>{story.name}</Text>
                <Text>Estimación: {story.estimate || 'Sin estimación'}</Text>
                <Text>Prioridad: {story.priority}</Text>
                <Text>Estado: {story.status}</Text>
                <Text>Horas trabajadas: {story.worked_hours}</Text>
              </View>
            ))
          }
        </View>
      </Page>
    </Document>
  );
};

export default ReporteSprintComponent;