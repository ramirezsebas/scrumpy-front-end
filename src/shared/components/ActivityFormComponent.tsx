import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import { Box, makeStyles, Theme } from '@material-ui/core';
import { useState, useContext } from 'react';
import { ActivityContext } from '../../data/providers/activity/activity-context';
import { Activity, ActivityInitializer } from '../interfaces/activity.interface';
import { ErrorContext } from '../../data/providers/error/error-context';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DayJsUtils from '@date-io/dayjs';
import dayjs from 'dayjs';

const useStyles = makeStyles((theme: Theme) => ({
    inputsContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    inputs: {
        marginBottom: 30,
    },
    inputsChildren: {
        marginBottom: 30,
        width: '48%',
    },
    button: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    },
    buttonDelete: {
        backgroundColor: "#FF0000",
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    },
}));

type activityProps =
    {
        open: boolean;
        cerrarModal: () => void;
        savedActivity?: Activity;
        handleSave?: (activity: Activity) => Promise<void>;
        handleUpdate?: (activity: Activity) => Promise<void>;
    }

export default function ActivityFormComponent(props: activityProps) {

    const [formData, setFormData] = useState<Activity>(props.savedActivity || ActivityInitializer);

    const classes = useStyles();

    const { state } = useContext(ActivityContext);

    const { dialog, setDialog, handleClose } = useContext(ErrorContext);

    const now = dayjs();
    
    const [selectedDate, handleDateChange] = useState(now);

    useEffect(() => {
        props.savedActivity && setFormData(props.savedActivity as Activity)
        props.savedActivity && handleDateChange(dayjs(props.savedActivity.created))
    }, [props.savedActivity]);

    const submit = async () => {
        if (formData.worked_hours <= 0) {
            setDialog({
                title: 'Atención ⚠️',
                body: "No se admiten Horas Trabajadas menores o iguales a 0",
                buttonLabel: 'Aceptar',
                open: true
            });
            return
        }
        try {
            let formDataPost: Activity = {
                ...formData,
                created: selectedDate.format("YYYY-MM-DD"),
            }
            if (props.handleSave) {
                await props.handleSave(formDataPost);
            }
            if (props.handleUpdate) {
                await props.handleUpdate(formDataPost);
            }
            setFormData(ActivityInitializer);
            props.cerrarModal();
        } catch (error) {
            console.log(error);
        }       
    }
    return (
        <div>
            <Dialog open={props.open} onClose={props.cerrarModal} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{!props.savedActivity ? "Nueva Actividad" : "Editar Actividad"}</DialogTitle>
                <DialogContent>
                    <Box>
                        <MuiPickersUtilsProvider utils={DayJsUtils} >
                            <DatePicker
                                label="Fecha de finalización"
                                value={selectedDate}
                                format="DD/MM/YYYY"
                                minDate={now}
                                onChange={(value) => {
                                    let date = value!;
                                    handleDateChange(date);
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Box>
                    <TextField
                        value={formData.description}
                        onChange={(event) => setFormData({ ...formData, description: event.target.value })}
                        required
                        autoFocus={!props.savedActivity}
                        margin="dense"
                        id="activityDescription"
                        label="Descripción"
                        placeholder="Descripción de la Actividad"
                        multiline
                        rows={4}
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                    />
                    <TextField
                        value={formData.worked_hours}
                        onChange={(event) => setFormData({ ...formData, worked_hours: Number(event.target.value) })}
                        margin="dense"
                        id="activityHours"
                        label="Horas trabajadas"
                        placeholder="Horas trabajadas en la Actividad"
                        type="number"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={submit} className={classes.button}>
                        {!props.savedActivity ? "Crear" : "Guardar"}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}