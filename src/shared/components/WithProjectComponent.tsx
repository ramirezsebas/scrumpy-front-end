import { FC, useContext, useState } from 'react';
import { postProyecto, setCurrentProject } from '../../data/providers/proyecto/proyecto-action-creators';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { Proyecto } from '../interfaces/proyecto.interface';
import CreateProjectFormComponent from './CreateProjectFormComponentModal';
import ProjectCard from './ProjectCard';
import { Link } from 'react-router-dom';

type Proyectos = {
    projectList: Proyecto[];
    open: boolean;
    handleOpen: () => void;
    handleClose: () => void;
    handleSave: (proyecto: Proyecto) => Promise<void>;
}

const WithProjectComponent: FC<Proyectos> = ({ projectList }) => {

    const { dispatch } = useContext(ProyectoContext);

    const [open, setOpen] = useState(false);
    const handleSave = async (proyecto: Proyecto) => {
        await postProyecto(dispatch, proyecto);
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            {
                projectList.map((element, index) => (
                    <Link
                        key={index} 
                        to={`/projectDetail/${element.id}/planning`} 
                        style={{textDecoration: 'none'}}
                        onClick={() => {
                            dispatch(setCurrentProject(element));
                        }}
                    > 
                        <ProjectCard key={index} project={element} />
                    </Link>
                ))
            }

            <CreateProjectFormComponent open={open} handleOpen={handleOpen} handleSave={handleSave} handleClose={handleClose} />
        </>
    );
}

export default WithProjectComponent;

