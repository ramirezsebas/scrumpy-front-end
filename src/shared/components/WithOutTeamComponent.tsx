import { makeStyles, Theme, createStyles, Button } from '@material-ui/core';
import { FC } from 'react';

import proyects from "../../assets/images/mis_proyectos.svg";
import { ProjectPermissionsCode } from '../../data/constants/permisos';
import PermissionsChecker from './PermissionsChecker';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '80px',
        },
        imgTeam: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
    }),
);

type WithOutTeamProps = {
    handleOpen: () => void
}

const WithOutTeamComponent: FC<WithOutTeamProps> = ({ handleOpen }) => {
    const classes = useStyles();

    return (
        <>
            <div className={classes.imgTeam}>
                <img src={proyects} alt="" width="50%" />
            </div>
            <div className={classes.buttonCreate}>
                <PermissionsChecker
                    tipo="Project"
                    permissions={[ProjectPermissionsCode.AGREGAR_MIEMBRO,]}
                >
                    <Button onClick={handleOpen} variant="contained" color="primary" component="span">
                        Agregar miembro
                    </Button>
                </PermissionsChecker>
            </div>
        </>
    )
}
export default WithOutTeamComponent;
