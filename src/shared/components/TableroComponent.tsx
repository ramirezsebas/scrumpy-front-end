import { FC, useState, useContext } from 'react';
import { Box, Button, Card, CardActions, CardContent, Container, createStyles, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, makeStyles, Tab, TableSortLabel, Theme, Tooltip, Typography } from '@material-ui/core'
import MoveToInboxIcon from '@material-ui/icons/MoveToInbox';
import { Draggable, Droppable } from 'react-beautiful-dnd'
import { UserStory } from '../interfaces/user_story.interface';
import { AddCircleOutlined, Done, DoneAllOutlined, FilterListOutlined, SortOutlined } from '@material-ui/icons';
import { putUserStory } from '../../data/providers/userstory/user-story-action-creators';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { SprintContext } from '../../data/providers/sprint/sprint-context';
import { ErrorContext } from '../../data/providers/error/error-context';
import DialogComponent from './DialogComponent';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        'card-': {
            display: "flex",
            justifyContent: 'space-between',
            alignItems: 'start',
            cursor: 'pointer',
            marginTop: "5px",
            marginBottom: "5px",
        },
        'add-us': {
            display: "flex",
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        'list-card': {
            height: '400px',
        }
    }),
);

type Tablero = {
    id: string;
    listTiles: UserStory[];
    title: string;
    canAdd?: boolean;
    canSort?: boolean;
    canFilter?: boolean;
    onFilter?: () => void;
    estadosFiltrados?: any;
    onOpen?: () => void;
    onSort?: (list: UserStory[]) => void;
    handleClick?: (item: UserStory) => void
}

const TableroComponent: FC<Tablero> = ({ title, onSort, estadosFiltrados, onFilter, canFilter = false, canAdd = false, id, canSort = false, listTiles = [], onOpen, handleClick }) => {
    const classes = useStyles();

    const { state: { currentSprint } } = useContext(SprintContext);

    const { dispatch: storyDispatch } = useContext(UserStoryContext);

    const { state: { currentProject } } = useContext(ProyectoContext);

    const [openAlertDialog, setOpenAlertDialog] = useState(false);

    const { dialog, setDialog, handleClose: closeError } = useContext(ErrorContext);

    const [usClick, setUsClick] = useState<UserStory | undefined>(undefined);

    const handleClickOpen = () => {
        setOpenAlertDialog(true);
    };

    const handleClickClose = () => {
        setOpenAlertDialog(false);
        setUsClick(undefined);
    };

    const handleCardClick = (item: UserStory) => {
        if (handleClick) {
            handleClick(item);
        }
    }

    const handleCardArchiveClick = (item: UserStory) => {
        console.log(currentSprint.status);
        if (currentSprint.status === 'INICIADO') {
            setDialog({
                title: 'Atención ⚠️',
                body: 'No puedes archivar user stories en un sprint iniciado',
                buttonLabel: 'Aceptar',
                open: true
            });
        } else {
            setUsClick(item);
            handleClickOpen();
        }
    }

    return (
        <Container style={{ display: 'flex', flexDirection: 'column', height: '450px' }}>
            <Container style={{ marginTop: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Typography variant="h5">{title}</Typography>
                {canAdd && <Tooltip title="Agregar US"><Button disabled={currentProject.status === 'FINALIZADO'} onClick={onOpen}><AddCircleOutlined /></Button></Tooltip>}
                {canSort && onSort && <Tooltip title="Ordenar US Por Prioridad"><Button onClick={() => onSort(listTiles)}><SortOutlined /></Button></Tooltip>}
                {canFilter && onFilter && <Tooltip title="Filtrar US por su Estado"><Button onClick={() => onFilter()}><FilterListOutlined /></Button></Tooltip>}
            </Container>
            <Droppable droppableId={id}>
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        style={{
                            backgroundColor: snapshot.isDraggingOver ? '#f5f5f5' : '#fff',
                            padding: 4,
                            width: '100%',
                            height: '100%',
                            overflow: 'auto',
                        }}
                    >
                        {listTiles.map((item, index) => (
                            <Draggable key={item.id} draggableId={String(item.id)} index={index}>
                                {(provided) => (
                                    <Card className={classes['card-']} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                        <CardContent onClick={() => handleCardClick(item)}>
                                            {item.status === 'DONE' && <Typography> <Done /> </Typography>}
                                            <Typography>{item.name}</Typography>
                                            <Typography>Estimación: {item.estimate ? item.estimate : "Sin estimación"}</Typography>
                                            <Typography>Prioridad: {item.priority}</Typography>
                                            <Typography>Estado: {item.status}</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Grid
                                                container
                                                direction="column"
                                                justifyContent="space-between"
                                                alignItems="center"
                                            >
                                                <Grid item>
                                                    <Box mt={1}>
                                                        <Typography>#{item.id}</Typography>
                                                    </Box>
                                                </Grid>
                                                {
                                                    item.status !== "DONE" && item.status !== "ARCHIVED" &&
                                                    <Grid item>
                                                        <Box mt={5}>
                                                            <Button disabled={currentProject.status === "FINALIZADO"} size="small" onClick={() => handleCardArchiveClick(item)}>
                                                                <MoveToInboxIcon />
                                                            </Button>
                                                        </Box>
                                                    </Grid>
                                                }
                                            </Grid>
                                        </CardActions>
                                    </Card>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
            <Dialog
                open={openAlertDialog}
                keepMounted
                onClose={handleClickClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">{"Atención"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        ¿Estas seguro que desea archivar el user story #{usClick?.id}?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClickClose} color="secondary">
                        NO
                    </Button>
                    <Button onClick={async () => {
                        await putUserStory(storyDispatch, {
                            id: usClick?.id,
                            status: 'ARCHIVED',
                        }, currentProject.id);
                        handleClickClose();
                    }} color="primary">
                        SI
                    </Button>
                </DialogActions>
            </Dialog>
            <DialogComponent dialog={dialog} handleClose={closeError} />
        </Container>
    )
}
export default TableroComponent;