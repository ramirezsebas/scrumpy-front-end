import { makeStyles, Theme, createStyles, Typography } from '@material-ui/core';
import { FC } from 'react';
import noTasks from "../../assets/images/no-tasks.svg";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        myProyects: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-evenly'
        },
        buttonCreateContainer: {
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        buttonCreate: {
            padding: 10,
            paddingLeft: 30,
            paddingRight: 30,
        },
        imgProject: {
            marginTop: 50,
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        content: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
        },
        imageDescription: {
            fontSize: 21,
        }
    }),
);


const WithoutTaskComponent: FC = () => {
    const classes = useStyles();
    
    return (
        <>
            <div className={classes.imgProject}>
                <img src={noTasks} alt="" width="70%" />
            </div>
            <div className={classes.buttonCreateContainer}>
                <Typography component="h3" className={classes.imageDescription}>
                    Aún no tienes tareas asignadas
                </Typography>
            </div>
        </>
    )
}
export default WithoutTaskComponent;
