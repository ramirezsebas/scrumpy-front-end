import { Card, CardHeader, CardContent, Typography, Avatar, createStyles, makeStyles, Theme, CardActions } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import { FC, useContext, useEffect, useState } from 'react';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { Proyecto } from '../interfaces/proyecto.interface';
import equipoAPI from '../../api/memberScrumpyAPI';
import { Member, MemberInitializer } from '../interfaces/miembro.interface';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxWidth: 200,
        },
        media: {
            height: 0,
            paddingTop: '56.25%', // 16:9
        },
        padding:{
            left:'10',
            right:'10',
            top:'10',
            bottom:'10'
        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
            }),
        },
        cardFooter: {
            display: 'flex',
            justifyContent: 'flex-end'
        },
        expandOpen: {
            transform: 'rotate(180deg)',
        },
        avatar: {
            backgroundColor: red[500],
        },
        avatarExcedentes: {
            backgroundColor: '#ccc',
            color: theme.palette.secondary.main,
        },
    }),
);

type ProyectoCard = {
    project:Proyecto;
}

const ProjectCard:FC<ProyectoCard> = ({project}) => {
    const classes = useStyles();
    const [miembros, setMiembros] = useState<Member[]>([]);

    useEffect(() => {
        (async () => {
            let result = await equipoAPI.getAllMiembros(project.id as number);
            setMiembros(result);
        })();
    }, []);

    return (
        <Card style={{padding:'10px',margin:'20px'}} className={classes.root}>
            <CardHeader
                title={project.name + (project.status === 'FINALIZADO' ? ' (F)' : '')}
                subheader=""
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary">
                    {project.description}
                </Typography>
            </CardContent>
            <CardActions className={classes.cardFooter}>
                {
                    miembros?.slice(0, 2).map(miembro => {
                        if((miembro.user.picture?.length || 0) > 0)
                            return <Avatar key={miembro.id} aria-label="recipe" className={classes.avatar} src={miembro.user.picture} />
                        else
                            return (
                                <Avatar key={miembro.id} aria-label="recipe" className={classes.avatar}>
                                    { miembro.user.first_name[0] }
                                </Avatar>
                            )
                    })
                }
                {
                    miembros?.length >= 3 &&
                    <Avatar aria-label="recipe" className={classes.avatarExcedentes}>
                        +{ miembros.length - 2 }
                    </Avatar>
                }
            </CardActions>
        </Card>
    );
}

export default ProjectCard