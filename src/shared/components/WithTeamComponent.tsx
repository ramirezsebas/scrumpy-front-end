import React, { useState, FC, useContext } from 'react';
import { createStyles, Divider, makeStyles, Typography, Theme, Fab } from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import MiembroFormComponent from './MiembroFormComponent';
import ItemCardComponent from './ItemCardComponent';
import { Member } from '../interfaces/miembro.interface';
import { postMiembro, putMiembro, deleteMiembro } from '../../data/providers/miembro/miembro-action-creators';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { ProjectRolContext } from '../../data/providers/rolProyecto/rolProyecto-context';
import PermissionsChecker from './PermissionsChecker';
import { ProjectPermissionsCode } from '../../data/constants/permisos';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
    }),
);

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const WithTeamComponent: FC = () => {
    const classes = useStyles();

    const { state: { currentProject } } = useContext(ProyectoContext);

    const { state: { roles } } = useContext(ProjectRolContext);

    const { state: { miembros }, dispatch } = useContext(MiembroContext);


    const [miembroEdit, setMiembroEdit] = useState<Member | undefined>(undefined);

    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        sleep(100).then(() => {
            setMiembroEdit(undefined);
        });

    };

    const handleSave = async (miembroProp: Member) => {
        if (miembroEdit !== undefined) {
            await putMiembro(dispatch, miembroProp, currentProject.id as number);
        } else {
            await postMiembro(dispatch, miembroProp, currentProject.id as number);
        }
    }

    const handleDelete = async (miembroProp: Member) => {
        await deleteMiembro(dispatch, miembroProp, currentProject.id as number);
    }

    const editMiembro = (miembroProp: Member) => {
        setMiembroEdit(miembroProp);
        handleOpen();
    }

    return (
        <>
            {
                miembros.map((miembro) => {
                    return (
                        <ItemCardComponent
                            key={miembro.id}
                            upperLeft={miembro.user.email}
                            upperRight={miembro.availability.toString()}
                            lowerLeft={miembro.projectRole.name}
                            marginBottom={10}
                            clickable
                            onClick={() => {
                                editMiembro(miembro);
                            }}
                        />
                    )
                })
            }
            {currentProject.status !== "FINALIZADO" && <PermissionsChecker
                tipo="Project"
                permissions={[ProjectPermissionsCode.AGREGAR_MIEMBRO,]}
            >
                <Fab
                    color="primary"
                    className={classes.addButton}
                    onClick={() => {
                        setMiembroEdit(undefined);
                        handleOpen();
                    }}
                >
                    <AddIcon />
                </Fab>
            </PermissionsChecker>

            }
            <MiembroFormComponent open={open} rolList={roles} miembro={miembroEdit} save={handleSave} delete={handleDelete} cerrarModal={handleClose} />
        </>
    );
}

export default WithTeamComponent;