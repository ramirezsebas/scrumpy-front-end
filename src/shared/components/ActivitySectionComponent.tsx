import React, { FC, useCallback, useContext, useState } from 'react'
import { Box, Button, makeStyles, TextField, Theme, Typography } from '@material-ui/core'
import { ActivityContext } from '../../data/providers/activity/activity-context';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import ActivityCardComponent from './ActivityCardComponent';
import { postActivity, putActivity } from '../../data/providers/activity/activity-action-creators';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import { Activity, ActivityInitializer } from '../interfaces/activity.interface';
import ActivityFormComponent from './ActivityFormComponent';
import { AuthContext } from '../../data/providers/auth/auth-context';

const useStyles = makeStyles((theme: Theme) => ({
    button: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    }
}));


const ActivitySectionComponent = () => {
    const { state: { activities, loading }, dispatch } = useContext(ActivityContext);
    const [open, setOpen] = useState<string | false>(false);
    const classes = useStyles();
    const { state: { currentMember } } = useContext(MiembroContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);
    const [savedActivity, setSavedActivity] = useState<Activity>(ActivityInitializer);
    const { state: { miembros } } = useContext(MiembroContext);
    const { state: { auth } } = useContext(AuthContext);

    const submitActivity  = async (activity: Activity) => {
        await postActivity(dispatch, currentProject.id as number, currentUS.id as number, activity);
    }
    const updateActivity = async (activity: Partial<Activity>) => {
        await putActivity(dispatch, currentProject.id, currentUS.id, activity);
    }

    const getMiembroById = useCallback((id: number) => {
		return miembros.find(miembro => miembro.id === id);
	}, [miembros]);

    const miembroVsUser = () => {
		return getMiembroById(currentUS.member)?.user.id === auth.usuario.id;
	}

    return (
        <>
            <Box
                style={{
                    height: '75%',
                    overflowY: 'scroll',
                    overflowX: 'hidden',
                }}
            >
                {
                    !loading && activities.map(activity => (
                        <ActivityCardComponent 
                        key={activity.id} 
                        activity={activity}
                        clickable
                        onClick={() => {
                            console.log("ni entri")
                            setSavedActivity({
                                id: activity.id,
                                sprint: activity.sprint,
                                user_story: activity.user_story,
                                user: activity.user,
                                description: activity.description,
                                worked_hours: activity.worked_hours,
                                created: activity.created,
                            });
                            /*{setOpen('edit');}*/
                        }}/>
                    ))
                }
            </Box>

            {
                currentProject.status !== "FINALIZADO" && miembroVsUser() && currentUS.status === 'DOING' &&
                <Button onClick={() => setOpen('create')} className={classes.button}>
                            {"Cargar Actividad"}
                </Button>
            }
            <ActivityFormComponent handleSave={submitActivity} open={open === 'create'} cerrarModal={() => setOpen(false)} />
            <ActivityFormComponent handleUpdate={updateActivity} open={open === 'edit'} cerrarModal={() => setOpen(false)} savedActivity={savedActivity} />
        </>
    )
}

export default ActivitySectionComponent
