import { Drawer, Box, Typography, Divider, List, ListItem, ListItemIcon, ListItemText, createStyles, makeStyles, Theme } from '@material-ui/core'
import { useContext } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { actionList, projectActionList } from '../routes/nav_routes';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { AuthContext } from '../../data/providers/auth/auth-context';
import { doLogout } from '../../data/providers/auth/auth-action-creators';
import PermissionsChecker from './PermissionsChecker';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { SprintContext } from '../../data/providers/sprint/sprint-context';

const drawerWidth = 240;


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
        activeMenu: {
            backgroundColor: theme.palette.primary.main,
        }
    }),
);

export default function SystemDrawerComponent() {
    const classes = useStyles();
    const { dispatch } = useContext(AuthContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentSprint } } = useContext(SprintContext);
    const location = useLocation();
    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
            anchor="left"
        >
            <Box textAlign="center" mt={5} mb={5}>
                <Link to="/" style={{ textDecoration: 'none', color: "#302929" }}>
                    <Typography>
                        SCRUMPY
                    </Typography>
                </Link>
            </Box>
            <Divider />
            <List>
                {!(location.pathname.includes('projectDetail')) &&
                actionList.map((item,index) => {
                    return (

                        <PermissionsChecker key={index} permissions={item.permisos}>
                            <Link key={item.title} to={item.link} style={{ textDecoration: 'none', color: "#302929" }}>
                                <ListItem button style={location.pathname === item.link ? {backgroundColor: '#6200EE', color: '#fff'} : {}} >
                                    <ListItemIcon>{<item.icon />}</ListItemIcon>
                                    <ListItemText primary={item.title} />
                                </ListItem>
                            </Link>
                        </PermissionsChecker>
                    )
                }
                )}
                {
                    (location.pathname.includes('projectDetail')) &&
                    projectActionList.map((item,index) => {
                        return (
                            <PermissionsChecker key={index} permissions={item.permisos}>
                                <Link key={item.title} to={`/projectDetail/${currentProject.id}${item.link}`} style={{ textDecoration: 'none', color: "#302929" }}>
                                    <ListItem button style={location.pathname.includes(item.link) ? {backgroundColor: '#6200EE', color: '#fff'} : {}}>
                                        <ListItemIcon>{<item.icon />}</ListItemIcon>
                                        <ListItemText primary={item.title === 'Sprint' ? `Sprint ${currentSprint.iteration}` :  item.title} />
                                    </ListItem>
                                </Link>
                            </PermissionsChecker>
                        )
                    }
                )}
            </List>
            <Divider />
            <List>
                <ListItem button onClick={() => {
                    dispatch(doLogout())
                }}>
                    <ListItemIcon><ExitToAppIcon /></ListItemIcon>
                    <ListItemText>Logout</ListItemText>
                </ListItem>
            </List>
        </Drawer>
    )
}
