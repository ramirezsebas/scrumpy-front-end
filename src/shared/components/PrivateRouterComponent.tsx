import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { AuthContext } from "../../data/providers/auth/auth-context";

const PrivateRouter: React.FC<{
    component: React.FC;
    path: string;
    exact: boolean;
}> = (props) => {

    const { state: { initialized, auth: { loggedIn } } } = useContext(AuthContext);

    console.log(`${props.path}: ` + (initialized && loggedIn));
    return (initialized && loggedIn) ? (<Route path={props.path} exact={props.exact} component={props.component} />) :
    (<Redirect to="/" />);
};
export default PrivateRouter;