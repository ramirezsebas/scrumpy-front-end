import React, { FC, useCallback, useContext, useState } from 'react'
import { Box, Button, makeStyles, TextField, Theme, Typography } from '@material-ui/core'
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import { AuthContext } from '../../data/providers/auth/auth-context';
import { EventUSContext } from '../../data/providers/eventUS/eventUS-context';
import { EventUS, EventUSInitializer } from '../interfaces/eventUS.interface';
import EventUSCardComponent from './EventCardComponent';

const useStyles = makeStyles((theme: Theme) => ({
    button: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    }
}));


const EventUSSectionComponent = () => {
    const { state: { events, loading }, dispatch } = useContext(EventUSContext);
    const [open, setOpen] = useState<string | false>(false);
    const classes = useStyles();
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);
    const [savedEventUS, setSavedEventUS] = useState<EventUS>(EventUSInitializer);
    const { state: { auth } } = useContext(AuthContext);

    return (
        <>
            <Box
                style={{
                    height: '75%',
                    overflowY: 'scroll',
                    overflowX: 'hidden',
                }}
            >
                {
                    !loading && events.map(event => (
                        <EventUSCardComponent 
                        key={event.id} 
                        event={event}
                        />
                    ))
                }
            </Box>
        </>
    )
}

export default EventUSSectionComponent
