import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button } from '@material-ui/core';
import { FC } from 'react'

type CreateUS = {
    open: boolean,
    handleClose: () => void,
    handleOpen: () => void,
    handleName: (event: React.ChangeEvent<HTMLInputElement>) => void,
    handleDescription: (event: React.ChangeEvent<HTMLInputElement>) => void,
    handlePriority: (event: React.ChangeEvent<HTMLInputElement>) => void,
    createUS: () => void
}

export const CreateUSDialog: FC<CreateUS> = ({ open, handleClose, handleName, handleDescription, handlePriority, createUS }) => {
    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Crear User Story</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Crear User Story
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    variant="outlined"
                    id="nombre"
                    onChange={handleName}
                    label="Nombre"
                    type="text"
                    fullWidth />
                <TextField
                    margin="dense"
                    variant="outlined"
                    id="descripcion"
                    multiline
                    rows={4}
                    onChange={handleDescription}
                    label="Descripcion"
                    type="text"
                    fullWidth />
                <TextField
                    margin="dense"
                    variant="outlined"
                    id="prioridad"
                    onChange={handlePriority}
                    label="Prioridad"
                    type="number"
                    InputProps={{ inputProps: { min: 0, max: 10 } }}
                    fullWidth />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={createUS} color="primary">
                    Crear
                </Button>
            </DialogActions>
        </Dialog>
    )
}
