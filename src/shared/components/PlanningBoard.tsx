import { Button, Checkbox, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, FormControlLabel, FormGroup, FormHelperText, FormLabel, makeStyles, Modal, TextField, Typography } from '@material-ui/core'
import React, { FC, useContext, useEffect, useState } from 'react'
import { DragDropContext, DropResult, ResponderProvided } from 'react-beautiful-dnd'
import userStoryAPI from '../../api/userStoryScrumpyAPI'
import { ProjectPermissionsCode } from '../../data/constants/permisos'
import { ErrorContext } from '../../data/providers/error/error-context'
import { MiembroContext } from '../../data/providers/miembro/miembro-context'
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context'
import { ProjectRolContext } from '../../data/providers/rolProyecto/rolProyecto-context'
import { SprintContext } from '../../data/providers/sprint/sprint-context'
import { postUserStory, setCurrentUS, updateUserStory } from '../../data/providers/userstory/user-story-action-creators'
import { UserStoryContext } from '../../data/providers/userstory/user-story-context'
import { UserStory, userStoryInitializer } from '../interfaces/user_story.interface'
import { CreateUSDialog } from './CreateUSDialog'
import TableroComponent from './TableroComponent'
import UserStoryCardComponent from './UserStoryCardComponent'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing(3),
    },
}));


export const PlanningBoard = (props: any) => {
    const classes = useStyles();
    const { setProductBacklog, setSprintBacklog, sprintBacklog, productBacklog } = props
    const { state: { currentProject }, dispatch: projectDispatch } = useContext(ProyectoContext);
    const { state: { userStories }, dispatch: dispatch } = useContext(UserStoryContext);
    const { state: { currentSprint }, dispatch: sprintDispatch } = useContext(SprintContext);
    const { state: { miembros } } = useContext(MiembroContext);
    const { dialog, setDialog, handleClose } = useContext(ErrorContext);
    const { hasPermission } = useContext(ProjectRolContext);
    const [formData, setFormData] = useState(userStoryInitializer);

    const [open, setOpen] = useState(false);
    const [ascend, setAscend] = useState(false);
    const [openUSCard, setOpenUSCard] = useState(false);
    const [openFilter, setOpenFilter] = useState(false);
    const [savedUS, setSavedUS] = useState<UserStory>(userStoryInitializer);

    const [filterState, setFilterState] = React.useState({
        all: true,
        new: false,
        unfinished: false,
        done: false,
    });



    const handleClickUS = (item: UserStory) => {
        setSavedUS(item);
        setOpenUSCard(true);
        dispatch(setCurrentUS(item));
    }

    const handleChangeCheck = (event: any) => {
        setFilterState({ ...filterState, [event.target.name]: event.target.checked });
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClickClose = () => {
        setOpen(false);
    };

    const handleChange = (pb: UserStory[], sb: UserStory[]) => {
        setProductBacklog(pb);
        setSprintBacklog(sb);
    }

    const handleName = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        setFormData((prev_state: any) => {
            return {
                ...prev_state,
                name: event.target.value,
                project: currentProject.id,
            }
        });
    }

    const handleDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();

        setFormData((prev_state: any) => {
            return {
                ...prev_state,
                description: event.target.value,
                project: currentProject.id,
            }
        });
    }

    const createUS = async () => {
        //Create User Story
        console.log(formData);
        console.log("Crear US");
        try {
            await postUserStory(dispatch, formData);

            console.log('creado');
        } catch (error: any) {
            alert(error.message)
        }


        setOpen(false);
    }

    const onFilter = () => {
        setOpenFilter(true);

    }

    const onCloseFilter = () => {
        console.log('filterState');
        console.log(filterState);
        var filteredList = userStories.filter((item: UserStory) => {
            if (filterState.all) {
                console.log('aninati');
                return item;
            }
            console.log("newww");

            console.log(item.description);
            console.log(item.status);
            console.log();
            if (filterState.new && item.status === "NEW") {
                console.log('entro en new');
                return item;
            } else if (filterState.unfinished && item.status === "UNFINISHED") {
                return item;
            } else if (filterState.done && item.status === "DONE") {
                return item;
            }

        }
        );
        console.log('filteredList');
        console.log(filteredList);

        setProductBacklog(filteredList);
        setOpenFilter(false);
    }


    const onSort = (list: UserStory[]) => {

        var sortedList = ascend ? list.sort((a: UserStory, b: UserStory) => {
            return a.priority > b.priority ? -1 : 1;
        }) : list.sort((a: UserStory, b: UserStory) => {
            return a.priority > b.priority ? 1 : -1;
        });

        setProductBacklog(sortedList);
        setAscend(!ascend);
    }

    const handlePriority = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        setFormData((prev_state: any) => {
            return {
                ...prev_state,
                priority: event.target.value,
                project: currentProject.id,
            }
        });

    };



    function handleOnDragEnd(result: DropResult, provided: ResponderProvided) {
        const { draggableId, destination, source, combine } = result;

        if (currentProject.status === 'FINALIZADO') {
            setDialog({
                title: 'Atención ⚠️',
                body: `Ya se ha finalizado el proyecto no puedes mover!`,
                buttonLabel: 'Aceptar',
                open: true
            });
            return;
        }
        //Verifican si esta en el Mismo Tablero
        if (destination?.droppableId === source.droppableId) {
            console.log('Moviendo en la Misma Lista')
            reorderCurrentList();
            return
        }

        if (source.droppableId === 'product-log' && destination?.droppableId === 'sprint-log') {
            console.log('Moviendo de PB a SB')
            if (!hasPermission(ProjectPermissionsCode.PLANIFICAR_SPRINT)) {
                setDialog({
                    title: 'Atención ⚠️',
                    body: 'No tienes permiso para planificar el sprint!',
                    buttonLabel: 'Aceptar',
                    open: true
                });
                return;
            }
            //Si se mueve de product backlog a sprint backlog
            if (currentSprint.status === 'EN_PLANIFICACION' || currentSprint.status === '') {
                const newSprintBacklog = [...sprintBacklog];
                const newProductBacklog = [...productBacklog];
                const [removed] = newProductBacklog.splice(source.index, 1);
                if (removed.status === 'DONE' || removed.status === 'ARCHIVED') {
                    setDialog({
                        title: 'Atención ⚠️',
                        body: `El story seleccionado está ${removed.status === 'DONE' ? 'finalizado' : 'archivado'}!`,
                        buttonLabel: 'Aceptar',
                        open: true
                    });
                    return;
                }
                if (removed.estimate) {
                    newSprintBacklog.splice(destination.index, 0, removed);
                    userStoryAPI.moverUserStoryASB(currentProject.id, currentSprint.id, removed.id)
                        .then((updated: any) => {
                            console.log(updated)
                            dispatch(updateUserStory(updated));
                        });
                    handleChange(newProductBacklog, newSprintBacklog);
                } else {
                    let id = removed.id;
                    setDialog({
                        title: 'Atención ⚠️',
                        body: `Debes estimar el story #${id} para agregarlo al sprint backlog!`,
                        buttonLabel: 'Aceptar',
                        open: true
                    });
                }

            } else {
                setDialog({
                    title: 'Atención ⚠️',
                    body: 'No puedes agregar user stories a un sprint iniciado!',
                    buttonLabel: 'Aceptar',
                    open: true
                });
            }
            return;
        }

        if (source.droppableId === 'sprint-log' && destination?.droppableId === 'product-log') {
            if (currentSprint.status === 'EN_PLANIFICACION' || currentSprint.status === '') {
                console.log('Moviendo de SB a PB');
                //Si se mueve de sprint backlog a product backlog
                const newSprintBacklog = [...sprintBacklog];
                const newProductBacklog = [...productBacklog];
                const [removed] = newSprintBacklog.splice(source.index, 1);
                newProductBacklog.splice(destination.index, 0, removed);
                userStoryAPI.moverUserStoryAPB(currentProject.id, currentSprint.id, removed.id)
                    .then((updated) => {
                        console.log(updated);
                        dispatch(updateUserStory(updated));
                    })
                handleChange(newProductBacklog, newSprintBacklog);
                return;
            } else {
                setDialog({
                    title: 'Atención ⚠️',
                    body: 'No puedes mover tu us en un sprint iniciado!',
                    buttonLabel: 'Aceptar',
                    open: true
                });
            }
        }

        function reorderCurrentList() {
            console.log(`Moviendo en el Tablero ${source.droppableId}`);
            const isProductBacklog = productBacklog.filter((item: any) => String(item.id) === draggableId)[0];
            const items: any = Array.from(isProductBacklog ? productBacklog : sprintBacklog);
            const [reorderedItem] = items.splice(result.source.index, 1);

            if (result.destination) {
                items.splice(result.destination.index, 0, reorderedItem);
                isProductBacklog ? handleChange(items, sprintBacklog) : handleChange(productBacklog, items);
            }
        }
    }

    console.log('pasando pb');
    console.log(productBacklog);

    return (
        <>
            <Container style={{ display: 'flex', height: '100px' }}>
                <DragDropContext onDragEnd={handleOnDragEnd}>
                    <TableroComponent
                        canAdd={hasPermission(ProjectPermissionsCode.CREAR_USER_STORY)}
                        canSort={true}
                        onSort={onSort}
                        canFilter={true}
                        onFilter={onFilter}
                        onOpen={handleClickOpen}
                        handleClick={handleClickUS}
                        listTiles={productBacklog}
                        title="Product Backlog"
                        id="product-log"
                    />
                    {

                        (props.sprintStatus === 'EN_PLANIFICACION' || props.sprintStatus === 'INICIADO') && <TableroComponent
                            onOpen={handleClickOpen}
                            handleClick={handleClickUS}
                            listTiles={sprintBacklog}
                            title="Sprint Backlog"
                            id="sprint-log"
                        />
                    }

                </DragDropContext>
                <CreateUSDialog
                    open={open}
                    handleClose={handleClickClose}
                    handleOpen={handleClickOpen}
                    handleName={handleName}
                    handleDescription={handleDescription}
                    handlePriority={handlePriority}
                    createUS={createUS}
                />
                <UserStoryCardComponent
                    isOpen={openUSCard}
                    handleClose={() => setOpenUSCard(false)}
                    readOnly={currentSprint.status === 'FINALIZADO'}
                />
            </Container>
            <Modal
                open={openFilter}
                onClose={onCloseFilter}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <Container style={{ width: '40%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', height: '55%', margin: '0 auto' }}>
                    <Typography variant="h5" component="h2">
                        Filtro
                    </Typography>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="legend">Por Cual Estado Deseas Filtrar?</FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox checked={filterState.all} onChange={handleChangeCheck} name="all" />}
                                label="All"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={filterState.new} onChange={handleChangeCheck} name="new" />}
                                label="New"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={filterState.unfinished} onChange={handleChangeCheck} name="unfinished" />}
                                label="Unfinished"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={filterState.done} onChange={handleChangeCheck} name="done" />}
                                label="Done"
                            />
                        </FormGroup>
                        <FormHelperText>Si seleccionas all vas a seleccionar todos</FormHelperText>
                    </FormControl>

                </Container>
            </Modal>
        </>
    )
}
