import React, { FC, useContext, useState } from 'react'
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, makeStyles, Radio, RadioGroup, Theme, Typography } from '@material-ui/core'
import { UserStory } from '../interfaces/user_story.interface';
import { RolContext } from '../../data/providers/rol/rol-context';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { Member } from '../interfaces/miembro.interface';

const useStyles = makeStyles((theme: Theme) => ({
	inputsContainer: {
		display: 'flex',
		justifyContent: 'space-between',
	},
	inputs: {
		marginBottom: 30,
	},
	inputsChildren: {
		marginBottom: 30,
		width: '48%',
	},
	button: {
		backgroundColor: theme.palette.primary.main,
		marginRight: 15,
		marginBottom: 15,
		marginTop: 0,
		color: '#FFFFFF',
		padding: 10,
	},
	activarContainer: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
	}
}));

type PickerProps = {
    open: boolean;
    handleClose: () => void;
    selected: Member | undefined;
    handleSelect: (miembro: Member) => void;
	handleAssign: () => Promise<void>;
}

const MemberPickerComponent: FC<PickerProps> = ({ open, handleClose, selected, handleSelect, handleAssign }) => {

	const { state: { miembros } } = useContext(MiembroContext);
    const classes = useStyles();

    return (
        <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Miembros</DialogTitle>
        <DialogContent>
            <RadioGroup>
                {miembros?.filter(miembro => miembro.is_active).map(miembro => (
                    <Box key={miembro.id} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Typography>{miembro.user.email}</Typography>
                        <Radio color="primary" checked={selected?.user.email === miembro.user.email} onChange={() => handleSelect(miembro)} />
                    </Box>
                ))}
            </RadioGroup>
        </DialogContent>
        <DialogActions>
            <Button 
				className={classes.button}
				onClick={async () => {
					handleClose();
					await handleAssign();
				}}
				disabled={!selected}
			>
				Asignar
			</Button>
        </DialogActions>
    </Dialog>
    )
}

export default MemberPickerComponent
