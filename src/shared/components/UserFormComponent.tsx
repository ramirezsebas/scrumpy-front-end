import { FC, useEffect, useState, useContext } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import { makeStyles, Theme } from '@material-ui/core';
import { Usuario, usuarioInitializer } from '../interfaces/usuario.interface';
import { RolContext } from '../../data/providers/rol/rol-context';
import { Role } from '../interfaces/role.interface';

const useStyles = makeStyles((theme: Theme) => ({
	inputsContainer: {
		display: 'flex',
		justifyContent: 'space-between',
	},
	inputs: {
		marginBottom: 30,
	},
	inputsChildren: {
		marginBottom: 30,
		width: '48%',
	},
	button: {
		backgroundColor: theme.palette.primary.main,
		marginRight: 15,
		marginBottom: 15,
		marginTop: 0,
		color: '#FFFFFF',
		padding: 10,
	},
	activarContainer: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
	} 
}));

type UserFormProps = {
	isOpen: boolean;
	handleClose: () => void,
	savedUser?: Usuario;
	handleSave?: (usuario:Usuario) => Promise<void>,
	handleUpdate?: (usuario:Usuario) => Promise<void>,
}

const  UserFormComponent: FC<UserFormProps> = (props) => {
	const { isOpen, handleClose, savedUser, handleSave, handleUpdate } = props;
	const [formData, setFormData] = useState<Usuario>(savedUser || usuarioInitializer);
	const { state: { roles } } = useContext(RolContext);

	const Allroles: Role[] = roles;
	const isCreate = !savedUser;

	useEffect(() => savedUser && setFormData(savedUser as Usuario), [savedUser]);

	const updateField = (field: string, value: any) => {
		setFormData({...formData, [field]: value});
	}

	const validateEmail = (email: string): boolean => {
		let pattern = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/i);
		return pattern.test(email);
	}

	const handleSubmit = async () => {
		console.log(JSON.stringify(formData));
		handleClose(); 
		if (isCreate && handleSave)
			await handleSave(formData);

		if (!isCreate && handleUpdate)
			await handleUpdate(formData);
		setFormData(usuarioInitializer);
	}

	const classes = useStyles();

	return (
		<Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle id="form-dialog-title">{isCreate ? 'Nuevo Usuario' : 'Editar Usuario'}</DialogTitle>
			<DialogContent>
				<TextField
					className={classes.inputs}
					autoFocus={isCreate}
					required
					id="user-email"
					label="Email"
					placeholder="Email del usuario"
					type="email"
					variant="outlined"
					value={formData.email}
					error={formData.email !== '' && !validateEmail(formData.email)}
					onChange={(event) => updateField('email', event.target.value)}
					fullWidth
				/>

				<TextField
					className={classes.inputs}
					required
					id="user-nombres"
					label="Nombres"
					placeholder="Nombres del usuario"
					type="text"
					variant="outlined"
					value={formData.first_name}
					onChange={(event) => updateField('first_name', event.target.value)}
					fullWidth
				/>

				<TextField
					className={classes.inputs}
					required
					id="user-apellidos"
					label="Apellidos"
					placeholder="Apellidos del usuario"
					type="text"
					variant="outlined"
					value={formData.last_name}
					onChange={(event) => updateField('last_name', event.target.value)}
					fullWidth
				/>

				<TextField
					className={classes.inputs}
					id="user-direccion"
					label="Dirección"
					placeholder="Dirección del usuario"
					type="text"
					variant="outlined"
					value={formData.address}
					onChange={(event) => updateField('address', event.target.value)}
					fullWidth
				/>
				<Box className={classes.inputsContainer}>
					<TextField

						id="user-telefono"
						label="Teléfono"
						placeholder="Número de teléfono del usuario"
						type="text"
						variant="outlined"
						value={formData.phone_number}
					onChange={(event) => updateField('phone_number', event.target.value)}
						className={classes.inputsChildren}
					/>

					<FormControl variant="outlined" className={classes.inputsChildren}>
						<InputLabel id="demo-simple-select-outlined-label">Rol</InputLabel>
						<Select
	
							labelId="demo-simple-select-outlined-label"
							id="demo-simple-select-outlined"
							label="Rol"
							value={formData.system_role}
							onChange={(event) => updateField('system_role', event.target.value)}
						>
							{
								Allroles.map(rol => (
									<MenuItem key={rol.id} value={rol.id}>{ rol.name }</MenuItem>
								))
							}
						</Select>
					</FormControl>
				</Box>

				<Box className={classes.activarContainer}>
					<Typography id="switch-activar">Activo</Typography>
					<Switch
						color="primary"
						edge="end"
						inputProps={{ 'aria-labelledby': 'switch-activar' }}
						onChange={(event) => updateField('is_active', !formData.is_active)}
						checked={formData.is_active}
					/>
				</Box>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleSubmit} className={classes.button}>
					Guardar
				</Button>
			</DialogActions>
		</Dialog>
	);
}

export default UserFormComponent;