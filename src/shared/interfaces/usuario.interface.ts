export interface Usuario {
    id: number;
	email: string;
	first_name: string;
	last_name: string;
	address: string;
	phone_number: string;
	system_role: number,
	is_active: boolean,
    picture?: string;
};

export const usuarioInitializer: Usuario = {
    id: 1,
    email: '',
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
    system_role: 1,
    is_active: false,
    picture: '',
};