export interface Dialog {
    title: string;
    body: string;
    open: boolean;
    buttonLabel: string;
}