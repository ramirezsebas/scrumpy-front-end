import { UserStory } from "./user_story.interface";

export interface PastSprints {
    sprint: number;
    user_story: UserStory;
    story_status: string;
}