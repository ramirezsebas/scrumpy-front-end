import { Proyecto, proyectoInitializer } from "./proyecto.interface";

export interface ProjectRole {
    id: number;
    name: string;
    description: string;
    project: Proyecto;
    permissions: number[];
}

export const roleInitializer: ProjectRole = {
    id: 0,
    name: '',
    description: '',
    project: proyectoInitializer,
    permissions: [],
}