import { Sprint, sprintInitializer } from "./sprint.interface";
import { UserStory, userStoryInitializer } from "./user_story.interface";
import { Usuario, usuarioInitializer } from "./usuario.interface";

export interface EventUS {
    id: number;
    user_story: number;
    user : Usuario;
    description: string;
    date: string;
}

export const EventUSInitializer: EventUS = {
    id: 0,
    user_story: 0,
    user: usuarioInitializer,
    description: "",
    date: "",
}