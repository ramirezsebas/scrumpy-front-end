import { Usuario } from "./usuario.interface";
export interface Proyecto {
    id: number;
    name: string;
    description: string;
    sprintDuration: number;
    scrum_master: string | Usuario;
    status: string;
    startDay: string;
    endDay: string;
}

export const proyectoInitializer: Proyecto = {
    id: 1,
    name: "",
    description: "",
    scrum_master: "",
    sprintDuration: 0,
    status: "",
    startDay: "",
    endDay: "",
};