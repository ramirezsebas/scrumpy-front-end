import { ProjectRole, roleInitializer } from "./projectRole.interface";
import { Proyecto, proyectoInitializer } from "./proyecto.interface";
import { Usuario, usuarioInitializer } from "./usuario.interface";

export interface Member {
    id: number;
    user: Usuario;
    project: Proyecto;
    projectRole: ProjectRole;
    is_active: boolean;
    availability: number;
}

export const MemberInitializer: Member = {
    id: 0,
    user: usuarioInitializer,
    project: proyectoInitializer,
    projectRole: roleInitializer,
    is_active: false,
    availability: 0.0,
}