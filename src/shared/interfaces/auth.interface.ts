import { UserStory } from "./user_story.interface";
import { Usuario } from "./usuario.interface";

export interface Auth {
    usuario: Usuario;
    tasks: UserStory[];
    loggedIn?: boolean;
    token?: string;
}
