export interface Permiso {
    id: number;
    code: number;
    name: string;
    description: string;
}

export const permisoInitializer = {
    id: 0,
    code: 0,
    name: '',
    description: '',
}