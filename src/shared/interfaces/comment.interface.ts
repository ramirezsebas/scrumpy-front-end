import { Member, MemberInitializer } from "./miembro.interface";
import { UserStory, userStoryInitializer } from "./user_story.interface";

export interface Comment {
    id: number;
    member: Member;
    user_story: UserStory;
    content: string;
    created: string;
    modified: string;
}

export const CommentInitializer: Comment = {
    id: 0,
    member: MemberInitializer,
    user_story: userStoryInitializer,
    content: "",
    created: "",
    modified: "",
}