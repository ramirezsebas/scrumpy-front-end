import { Proyecto, proyectoInitializer } from "./proyecto.interface";

export interface Sprint {
    id: number;
    sprint_goal: string;
    project: Proyecto;
    status: string;
    iteration: number;
    startDay: string;
    endDay: string;
}

export const sprintInitializer: Sprint = {
    id: 1,
    sprint_goal: "",
    project: proyectoInitializer,
    status: "",
    iteration: 1,
    startDay: "",
    endDay: "",
};