import { Sprint, sprintInitializer } from "./sprint.interface";
import { UserStory, userStoryInitializer } from "./user_story.interface";
import { Usuario, usuarioInitializer } from "./usuario.interface";

export interface Activity {
    id: number;
    user_story: number;
    sprint: number;
    user : Usuario;
    worked_hours: number;
    description: string;
    created: string;
}

export const ActivityInitializer: Activity = {
    id: 0,
    user_story: 0,
    sprint: 0,
    user: usuarioInitializer,
    worked_hours: 0,
    description: "",
    created: "",
}