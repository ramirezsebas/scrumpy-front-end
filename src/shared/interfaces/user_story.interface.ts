export interface UserStory {
    id: number;
    project: number;
    sprint: number | null;
    member: number;
    name: string;
    description: string;
    priority: number;
    estimate: number;
    worked_hours: number;
    status: string;
    finish_date: string;
}

export const userStoryInitializer: UserStory = {
    id: 1,
    project: 1,
    sprint: 1,
    member: 1,
    name: '',
    description: '',
    priority: 1,
    estimate: 1,
    worked_hours: 1,
    status: 'NEW',
    finish_date: '',
}