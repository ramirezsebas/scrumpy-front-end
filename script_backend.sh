# Script
# 1. Clonar el Repositorio ✅
# 2. Seleccionar Tag de Iteracion ✅
# 3. Seleccionar el entorno (Desarrollo, Produccion) ✅
# 3. Desplegar
# 4. Poblar la BD con Datos de Prueba

#!/bin/bash
function clonarProyecto() {
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
    echo "Deseas clonar el proyecto? y/N"
    read resp
    if [ $resp = "y" ] || [ $resp = "Y" ]; then
        git clone
        #Clonamos el proyecto
        echo "Clonando el proyecto..."
        git clone https://gitlab.com/gaonaricardo/scrumpy-back-end.git
        echo "Clonado completado"
    else
        echo "No se ha clonado el proyecto"
        exit 1
    fi
}


function crearCargarEnv() {
    touch .env
    echo 'DB_NAME_DEV=postgres' > .env
    echo 'DB_USER_DEV=postgres' >> .env
    echo 'DB_PASSWORD_DEV=postgres' >> .env
    echo 'DB_HOST_DEV=db' >> .env
    echo 'DB_PORT_DEV=5432' >> .env
    echo 'SECRET_KEY=)7=5$^w=rq-ysjm4!7((g+(pii)a7m4)jast8se&r#$&c+$dn7' >> .env
    echo 'CLIENT_ID=318530030530-hvv8pllgdbskr04clsacnd6qq4uchjng.apps.googleusercontent.com' >> .env
}

#Verificamos que envie por lo menos el tag que queremo cambiar
if [ $# -eq 2 ] 
    then
    clonarProyecto
    cd scrumpy-back-end
    echo "Los Tags son:"
    git tag -l | grep -w $1

    if [ ! $? -eq 0 ] 
        then
        echo "Tag $1 No Existe, Finalizando el programa"
        exit 1
    fi
    echo "Iremos al tag '$1' en 'Master'"
    git checkout $1
else
    echo "Usage: $0 <tag> <entorno>"
    echo "Example: $0 iteracion1 produccion"
    echo "Example: $0 iteracion_3 desarrollo"
    exit 1
fi
if [  $2 = "desarrollo" ]; then
    echo "Entorno: Desarrollo"
    echo
    crearCargarEnv
    docker-compose up -d --build
    sleep 3
    echo "Creacion del Super Usuario"
    #Poblamos la base de datos (Solo Permisos)
    docker exec -it scrumpy-back-end_web_1 python src/manage.py createsuperuser
    docker exec -it scrumpy-back-end_db_1 psql -U postgres -d postgres -f home/load_data.sql  
else
    #TODO: HACER EL DESPIEGUE EN PRODUCCION
    echo "Entorno: Produccion"
    heroku login
    # curl --location --request POST 'https://poli-scrumpy-api.herokuapp.com/rellenar'
    git push heroku master
    heroku open
fi