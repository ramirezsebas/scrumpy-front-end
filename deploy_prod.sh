#Clonamos el proyecto
echo "Clonando el proyecto..."
git clone https://gitlab.com/ramirez-sebas1010/scrumpy-front-end.git
echo "Clonado completado"
bash archivo.sh 
#Verificamos que envie por lo menos el tag que queremo cambiar
if [ $# -lt 1 || $# -gt 1 ]; then
    echo "Usage: $0 <tag>"
    echo "Example: $0 iteracion1"
    echo "Example: $0 iteracion_3"
    exit 1
fi


#Cambiamos de rama
echo "Cambiando de rama..."
git checkout master
echo "Cambio de rama completado"

#Deploy to vercel
echo "Deploying to vercel..."
vercel --prod
echo "Deploying to vercel completado"