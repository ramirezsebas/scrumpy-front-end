#!/bin/bash

sudo chmod 777 -R .
rm -rf scrumpy-front-end
rm -rf scrumpy-back-end

echo "Creando el Backend "
bash script_backend.sh $1 $2

if [ ! $? -eq 0 ]; then
    echo "Error al crear el Backend"
    exit 1
fi

echo "Creando el Frontend"
bash script_frontend.sh $1 $2

if [ ! $? -eq 0 ]; then
    echo "Error al crear el Frontend"
    exit 1
fi

echo "Se ha creado exitosamente el proyecto"



echo
echo "Creando el Frontend "
bash script_frontend.sh $1 $2