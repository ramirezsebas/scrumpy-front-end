# Script
# 1. Clonar el Repositorio ✅
# 2. Seleccionar Tag de Iteracion ✅
# 3. Seleccionar el entorno (Desarrollo, Produccion) ✅
# 3. Desplegar
# 4. Poblar la BD con Datos de Prueba

#!/bin/bash
function clonarProyecto() {
    echo "Deseas clonar el proyecto? y/N"
    read resp

    if [ $resp = "y" ] || [ $resp = "Y" ]; then
        git clone
        #Clonamos el proyecto
        echo "Clonando el proyecto..."
        git clone https://gitlab.com/ramirez-sebas1010/scrumpy-front-end.git
        echo "Clonado completado"
    else
        echo "No se ha clonado el proyecto"
        exit 1
    fi
}

#Verificamos que envie por lo menos el tag que queremo cambiar
if [ $# -eq 2 ] 
    then
    clonarProyecto

    echo "Descargando el pm2"
    
    npm install pm2@latest -g
    pm2 list
    
    cd scrumpy-front-end

    echo "Descargando las Dependencias del proyecto"
    npm i --save
    
    echo "Los Tags son:"
    
    git tag -l | grep -w $1

    if [ ! $? -eq 0 ] 
        then
        echo "Tag $1 No Existe, Finalizando el programa"
        exit 1
    fi

    
    echo "Iremos al tag '$1'"
    git checkout $1
    

else
    echo "Usage: $0 <tag> <entorno>"
    echo "Example: $0 iteracion1 produccion"
    echo "Example: $0 iteracion_3 desarrollo"
    exit 1
fi
if [  $2 = "desarrollo" ]; then
    echo "Entorno: Desarrollo"
    echo "Iniciando"
    pm2 start --name scrumpy-front-end npm -- start
    # curl --location --request POST 'http://localhost:8080/rellenar'
else
    #TODO: HACER EL DESPIEGUE EN PRODUCCION
    echo "Entorno: Produccion"
    #Cambiamos de rama
    echo "Cambiando de rama..."
    git checkout master
    echo "Cambio de rama completado"

    #Deploy to vercel
    echo "Deploying to vercel..."
    vercel --prod
    echo "Deploying to vercel completado"
fi